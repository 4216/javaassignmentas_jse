package com.exception;

/**
 * 
 * @author Amani
 *
 */
 
public class UserNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -8924870519415522525L;
	private String massage;

	public UserNotFoundException() {
		super();
	}

	public UserNotFoundException(String massage) {
		super(String.format("  '%s'", massage));
		this.massage = massage;
	}

	public String getMassage() {
		return massage;
	}
}
