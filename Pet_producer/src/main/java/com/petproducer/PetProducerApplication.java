package com.petproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
//@EnableSwagger2
@EnableFeignClients
//@EnableEurekaClient
public class PetProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetProducerApplication.class, args);
	}

}
