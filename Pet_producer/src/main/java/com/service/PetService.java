package com.service;

import java.util.List;

import com.model.Pet;

/**
 * 
 * @author Amani
 *
 */
 
public interface PetService {

	public abstract Pet savePet(Pet pet);

	public abstract List<Pet> getAllPets();

	public abstract List<Pet> fetchAll();
	
	public abstract Pet buyPet(Pet pet);
	
	public abstract Pet getId(Long id);

}
