package com.service;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.model.UserDto;

/**
 * 
 * @author Amani
 *
 */
 
@FeignClient(name = "userConsumer", url = "http://localhost:9875/pets")
public interface UserService {
	/*
	 * @GetMapping("/") public abstract UserDto addUser(UserDto user);
	 * 
	 * public absstract UserDto updateUser(UserDto use);
	 * 
	 * public abstract List<UserDto> listUsers();
	 * 
	 * public abstract UserDto getUserById(Long id);
	 * 
	 * public abstract String removeUser(Long id);
	 * 
	 * public abstract UserDto findByUserName(String name);
	 * 
	 * public abstract Pet buyPet(Pet pet);
	 * 
	 * public abstract Optional<Pet> getMyPets(Long id);
	 * 
	 * public abstract UserDto loginUser(String name, String password, String
	 * confimPassword);
	 */

	@PostMapping("/add")
	public ResponseEntity<UserDto> addUser(@RequestBody @Valid UserDto user);

	@GetMapping("/login")
	public ResponseEntity<Object> loginUser(@RequestParam("name") String name1,
			@RequestParam("password") String password1, @RequestParam("confimPassword") String confimPassword1);

	@GetMapping("/logout")
	public ResponseEntity<String> logout();
}
