package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.model.Pet;
import com.service.PetService;

/**
 * 
 * @author Amani
 *
 */

@RestController
@RequestMapping("/pets")
public class PetPeersController {

	@Autowired
	private PetService petService;

	@GetMapping("/say")
	public ResponseEntity<String> sayHello() {
		return new ResponseEntity<String>("Welcome to Pets Consumer App.... ", HttpStatus.OK);
	}

	@GetMapping("/")
	public List<Pet> petHome() {
		return petService.getAllPets();
	}

	@GetMapping("/myPets")
	public ResponseEntity<List<Pet>> myPets() {
		return new ResponseEntity<List<Pet>>(petService.fetchAll(), HttpStatus.FOUND);
	}

	@GetMapping("/petDetail")
	public ResponseEntity<List<Pet>> petDetails() {
		return new ResponseEntity<List<Pet>>(petService.fetchAll(), HttpStatus.FOUND);
	}

	@PostMapping("/addPet")
	public ResponseEntity<Pet> addPet(@RequestBody Pet pet) {
		return new ResponseEntity<Pet>(petService.savePet(pet), HttpStatus.CREATED);
	}

	@PutMapping("/buyPet/{petId}")
	public Pet buyPet(@PathVariable Long petId) {
		return petService.getId(petId);
	}
}
