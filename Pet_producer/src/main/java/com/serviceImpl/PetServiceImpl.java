package com.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.model.Pet;
import com.repo.PetRepo;
import com.service.PetService;

/**
 * 
 * @author Amani
 *
 */

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepo petRepo;

	@Override
	@Transactional
	public Pet savePet(Pet pet) {
		Pet pet1 = null;
		if (pet != null) {
			pet1 = petRepo.save(pet);
		}
		return pet1;
	}

	@Override
	@Transactional
	public List<Pet> getAllPets() {
		return petRepo.findAll();
	}

	@Override
	@Transactional
	public List<Pet> fetchAll() {
		return petRepo.findAll();
	}

	@Override
	public Pet buyPet(Pet pet) {
		Optional<Pet> pet1 = petRepo.findById(pet.getId());
		Pet pet2 = pet1.get();
		Pet pet3 = petRepo.save(pet2);
//		petRepo.deleteById(pet.getId());
		return pet3;

	}

	@Override
	public Pet getId(Long id) {
		Optional<Pet> pet1 = petRepo.findById(id);
		Pet pet = pet1.get();
		return pet;
	}
}
