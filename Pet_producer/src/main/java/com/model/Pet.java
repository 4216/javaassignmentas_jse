package com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Amani
 *
 */
 
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PETS1")
public class Pet implements Serializable {
	private static final long serialVersionUID = 5915548474243144780L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", length = 5)
	private Long id;

	
	@Column(name = "PET_NAME", length = 55)
	private String name;

	
	@Column(name = "PET_AGE", length = 2, nullable = true)
	private Integer age;

	
	@Column(name = "PET_PLACE", length = 55)
	private String place;

	@JoinColumn(name = "PET_OWNERID")
//	@ManyToOne(fetch = FetchType.EAGER)
	private UserDto owner;
}
