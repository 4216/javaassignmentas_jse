package com.model;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Amani
 *
 */
 
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {

	private static final long serialVersionUID = -2363602923972309791L;

	private Long id;

	
	private String userName;

	
	private String userPassword;

	private transient String confirmPassword;

	@JsonIgnoreProperties({ "owner" })
	private Set<Pet> pets;
}
