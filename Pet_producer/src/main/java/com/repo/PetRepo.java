package com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.Pet;

/**
 * 
 * @author Amani
 *
 */

public interface PetRepo extends JpaRepository<Pet, Long> {

}
