package com.vo;

import java.io.Serializable;

public class EmployeeVO implements Serializable {
	private static final long serialVersionUID = -5130548362343603769L;
	private Integer empNo;

	private String empName;

	private Float salary;

	private String address;

	public EmployeeVO() {
		super();

	}

	public EmployeeVO(Integer empNo, String empName, Float salary, String address) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.address = address;
	}

	public Integer getEmpNo() {
		return empNo;
	}

	public void setEmpNo(Integer empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Float getSalary() {
		return salary;
	}

	public void setSalary(Float salary) {
		this.salary = salary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
