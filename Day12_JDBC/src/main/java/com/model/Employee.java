package com.model;

import java.io.Serializable;

public class Employee implements Serializable {
	private static final long serialVersionUID = 7842407163287688181L;

	private Integer empNo;

	private String empName;

	private Float salary;

	private String address;

	public Employee() {
		super();

	}

	public Employee(Integer empNo, String empName, Float salary, String address) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.address = address;
	}

	public Integer getEmpNo() {
		return empNo;
	}

	public void setEmpNo(Integer empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Float getSalary() {
		return salary;
	}

	public void setSalary(Float salary) {
		this.salary = salary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
