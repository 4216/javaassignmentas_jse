package com.exception;

/**
 * 
 * @author srikanth_penta
 *
 */
public class EmployeeException extends Exception {

	private static final long serialVersionUID = 1571290720689479404L;
	String str;

	public EmployeeException(String str) {
		super(str);
		this.str = str;
	}
}
