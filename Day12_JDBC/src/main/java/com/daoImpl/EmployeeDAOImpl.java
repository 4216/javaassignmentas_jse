package com.daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.dao.IEmployeeDAO;
import com.model.Employee;
import com.util.DBConnection;

public class EmployeeDAOImpl implements IEmployeeDAO {
	private static final String SELECT_Query_ID = "SELECT EMP_NO,EMP_NAME,SALARY,ADDRESSES FROM EMPLOYEE WHERE EMP_NO=?";
	private static final String SELECT_Query_Name = "SELECT EMP_NO,EMP_NAME,SALARY,ADDRESSES FROM EMPLOYEE WHERE EMP_NAME=?";
	private static final String SELECT_Query_ALL = "SELECT EMP_NO,EMP_NAME,SALARY,ADDRESSES FROM EMPLOYEE";
	private static final String UPDATE_Query = "UPDATE EMPLOYEE SET EMP_NAME=?,SALARY=?,ADDRESSES=? WHERE EMP_NO=?";
	private static final String DELETE_Query = "DELETE FROM EMPLOYEE WHERE EMP_NO=?";
	private static final String INSERT_Query = "INSERT INTO EMPLOYEE(EMP_NAME,SALARY,ADDRESSES) VALUES(?,?,?)";

	Connection connection;
	Statement st;
	PreparedStatement ps;

	ResultSet rs;
	Employee model;

	public EmployeeDAOImpl() throws SQLException {
		connection = DBConnection.getConnection();
	}

	@Override
	public String createEmployee(String str) throws SQLException {
		st = connection.createStatement();
		if (st != null && str != null) {
			st.executeUpdate(str);
			return "Created table in given database...";
		} else {
			return "Table is Not Created...";
		}
	}

	@Override
	public List<Employee> readAllEmployee() throws SQLException {
		ps = connection.prepareStatement(SELECT_Query_ALL);
		List<Employee> ls = new ArrayList<>();
		if (ps != null) {
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					Employee model1 = new Employee();
					model1.setEmpNo(rs.getInt(1));
					model1.setEmpName(rs.getString(2));
					model1.setSalary(rs.getFloat(3));
					model1.setAddress(rs.getString(4));
					ls.add(model1);
				}
			}
		}
		return ls;
	}

	@Override
	public Employee readEmployeeById(int id) throws SQLException {
		ps = connection.prepareStatement(SELECT_Query_ID);
		if (ps != null) {
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					model = new Employee();
					model.setEmpNo(rs.getInt(1));
					model.setEmpName(rs.getString(2));
					model.setSalary(rs.getFloat(3));
					model.setAddress(rs.getString(4));
				}
			}
		}
		return model;
	}

	@Override
	public Employee readEmployeeByName(String name) throws SQLException {
		ps = connection.prepareStatement(SELECT_Query_Name);
		if (ps != null) {
			ps.setString(1, name);
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					model = new Employee();
					model.setEmpNo(rs.getInt(1));
					model.setEmpName(rs.getString(2));
					model.setSalary(rs.getFloat(3));
					model.setAddress(rs.getString(4));
				}
			}
		}
		return model;
	}

	@Override
	public Employee updateEmployee(Employee model1) throws SQLException {
		ps = connection.prepareStatement(UPDATE_Query);
		int flag = 0;
		if (ps != null) {
			ps.setString(1, model1.getEmpName());
			ps.setFloat(2, model1.getSalary());
			ps.setString(3, model1.getAddress());
			ps.setInt(4, model1.getEmpNo());
			flag = ps.executeUpdate();
			if (flag == 0) {
				return null;
			} else {
				ps = connection.prepareStatement(SELECT_Query_ID);
				if (ps != null) {
					ps.setInt(1, model1.getEmpNo());
					rs = ps.executeQuery();
					if (rs != null) {
						while (rs.next()) {
							model = new Employee();
							model.setEmpNo(rs.getInt(1));
							model.setEmpName(rs.getString(2));
							model.setSalary(rs.getFloat(3));
							model.setAddress(rs.getString(4));
						}
					}
				}

			}
		}

		return model;
	}

	@Override
	public int deleteEmployee(int id) throws SQLException {
		ps = connection.prepareStatement(DELETE_Query);
		int flag = 0;
		if (ps != null) {
			ps.setInt(1, id);
			flag = ps.executeUpdate();
		}
		return flag;
	}

	@Override
	public int insertEmployee(Employee employee) throws SQLException {
		ps = connection.prepareStatement(INSERT_Query);
		int flag = 0;
		ps.setString(1, employee.getEmpName());
		ps.setFloat(2, employee.getSalary());
		ps.setString(3, employee.getAddress());
		flag = ps.executeUpdate();
		if (flag == 0) {
			flag = 0;
		} else {
			ps = connection.prepareStatement(SELECT_Query_Name);
			if (ps != null) {
				ps.setString(1, employee.getEmpName());
				rs = ps.executeQuery();
				if (rs != null) {
					while (rs.next()) {
						flag = rs.getInt(1);
					}
				}
			}
		}
		return flag;
	}
}
