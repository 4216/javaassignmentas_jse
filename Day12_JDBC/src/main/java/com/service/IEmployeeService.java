package com.service;

import java.sql.SQLException;
import java.util.List;

import com.model.Employee;
import com.vo.EmployeeVO;

public interface IEmployeeService {
	public String createEmployee(String str) throws SQLException;

	public List<Employee> readAllEmployee() throws SQLException;

	public Employee readEmployeeById(int id) throws SQLException;

	public Employee readEmployeeByName(String name) throws SQLException;

	public Employee updateEmployee(EmployeeVO e) throws SQLException;

	public int deleteEmployee(int id) throws SQLException;

	public int insertEmployee(EmployeeVO employeeVo) throws SQLException;

	public boolean validateEmployee(EmployeeVO vo);
}
