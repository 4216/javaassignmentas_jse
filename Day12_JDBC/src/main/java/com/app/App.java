package com.app;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import com.exception.EmployeeException;
import com.model.Employee;
import com.service.IEmployeeService;
import com.serviceImpl.EmployeeServiceImpl;
import com.vo.EmployeeVO;

/**
 * 
 * @author Amani
 *
 */
 
public class App {

	void controller() throws SQLException {
		IEmployeeService service = null;
		service = new EmployeeServiceImpl();

		Scanner sc = new Scanner(System.in);
		System.out.println("Select an option..");
		System.out.println("1.Insert");
		System.out.println("2.Delete");
		System.out.println("3.Select Employee By ID and Name");
		System.out.println("4.All Employee details");
		System.out.println("5.Update Employee details");
		System.out.println("6.Create new Table");
		System.out.println("7.Exit");
		int a = sc.nextInt();

		switch (a) {
		case 1:
			int insert = 0;
			insert = service.insertEmployee(new EmployeeVO(insert, "Amani", 1100.0f, "chenai"));
			if (insert == 0) {
				System.out.println("Employee Details are Not Inserted..");
			} else {
				System.out.println("Successfully Inserted  ID: " + insert);
			}
			controller();
			break;
		case 2:
			int delete = service.deleteEmployee(1);
			if (delete == 0) {
				System.out.println("EMPLOYEE ID IS NOT FOUND..");
			} else {
				System.out.println("Employee Is Deleted SuccessFully..");
			}
			controller();
			break;
		case 3:
			Employee dataId = service.readEmployeeById(2);
			System.out.println("Find By ID  " + dataId);

			Employee dataName = service.readEmployeeByName("sri");
			System.out.println("Find By Name  " + dataName);
			controller();
			break;

		case 4:
			System.out.println("-----------------ALL EMPLOYEE DETAILS ------------------");
			List<Employee> listData1 = service.readAllEmployee();
			if (listData1 != null) {
				System.out.println("EmpNo	| Name	| Salary	| Address");
				System.out.println("-------------------------------------------------------");
				for (Employee e : listData1) {
					System.out.println(e.getEmpNo() + "	| " + e.getEmpName() + "	| " + e.getSalary() + "	| "
							+ e.getAddress());
				}
			}
			controller();
			break;

		case 5:
			Employee update = service.updateEmployee(new EmployeeVO(15, "sripatel", 1200.22f, "hyd"));
			if (update != null) {
				System.out.println("Employee Details Are Updated Successfully...");
			} else {
				System.out.println("EMPLOYEE ID IS NOT FOUND..");
			}
			controller();
			break;

		case 6:
			String create = service.createEmployee(
					"CREATE TABLE REGISTRATION1 (id int not NULL PRIMARY KEY, first VARCHAR(255),last VARCHAR(255), age int)");
			System.out.println(create);
			controller();
			break;
		case 7:
			System.out.println("Exited...");
			System.exit(0);
			break;
		default:
			System.out.println("Exited...");
			break;
		}
		sc.close();
	}

	public static void main(String[] args) throws EmployeeException {
		App app = new App();
		try {
			app.controller();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			app = null;
		}
	}
}