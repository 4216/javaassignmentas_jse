package com.serviceImpl;

import java.sql.SQLException;
import java.util.List;

import com.dao.IEmployeeDAO;
import com.daoImpl.EmployeeDAOImpl;
import com.model.Employee;
import com.service.IEmployeeService;
import com.vo.EmployeeVO;

public class EmployeeServiceImpl implements IEmployeeService {
	IEmployeeDAO dao;

	public EmployeeServiceImpl() throws SQLException {
		dao = new EmployeeDAOImpl();
	}

	@Override
	public String createEmployee(String str) throws SQLException {
		String create = dao.createEmployee(str);
		return create;
	}

	@Override
	public List<Employee> readAllEmployee() throws SQLException {
		List<Employee> readAll = dao.readAllEmployee();
		return readAll;
	}

	@Override
	public Employee readEmployeeById(int id) throws SQLException {
		Employee dataId = dao.readEmployeeById(id);
		return dataId;
	}

	@Override
	public Employee readEmployeeByName(String name) throws SQLException {
		Employee dataName = dao.readEmployeeByName(name);
		return dataName;
	}

	@Override
	public Employee updateEmployee(EmployeeVO vo) throws SQLException {
		Employee model = new Employee();
		model.setEmpNo(vo.getEmpNo());
		model.setEmpName(vo.getEmpName());
		model.setSalary(vo.getSalary());
		model.setAddress(vo.getAddress());
		return dao.updateEmployee(model);
	}

	@Override
	public int deleteEmployee(int id) throws SQLException {
		int delete = dao.deleteEmployee(id);
		return delete;
	}

	@Override
	public int insertEmployee(EmployeeVO vo) throws SQLException {
		Employee model = new Employee();
		model.setEmpNo(vo.getEmpNo());
		model.setEmpName(vo.getEmpName());
		model.setSalary(vo.getSalary());
		model.setAddress(vo.getAddress());
		int insert = dao.insertEmployee(model);
		return insert;
	}

	@Override
	public boolean validateEmployee(EmployeeVO vo) {
		boolean flag = false;
		int length_No = String.valueOf(vo.getEmpNo()).length();
		if (length_No >= 1 || vo.getEmpNo()==0) {
			flag = true;
		}
		return flag;
	}
}
