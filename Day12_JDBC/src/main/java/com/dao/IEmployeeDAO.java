package com.dao;

import java.sql.SQLException;
import java.util.List;

import com.model.Employee;

public interface IEmployeeDAO {
	public String createEmployee(String str) throws SQLException;

	public List<Employee> readAllEmployee() throws SQLException;

	public Employee readEmployeeById(int id) throws SQLException;

	public Employee readEmployeeByName(String name) throws SQLException;

	public Employee updateEmployee(Employee employee) throws SQLException;

	public int deleteEmployee(int id) throws SQLException;

	public int insertEmployee(Employee employee) throws SQLException;
}
