package com.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.config.AppConfig;
import com.model.Car;

/**
 * 
 * @author Amani
 *
 */
 

public class App {

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
		Car car = ctx.getBean("car2", Car.class);
		System.out.println(car.getName());
		System.out.println(car.getNumber());

		((AbstractApplicationContext) ctx).close();
	}
}
