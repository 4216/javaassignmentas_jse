package com.service;

import org.springframework.stereotype.Component;

@Component("test1")
public class Test1 implements ITest {

	public void m1() {
		System.out.println("Test1.m1()");
	}

}
