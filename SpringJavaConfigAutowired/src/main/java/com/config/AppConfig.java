package com.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.model.Car;
import com.service.ITest;

@Configuration
@ComponentScan(basePackages = "com.sri.service")
public class AppConfig {
	@Autowired
	@Qualifier("test2")
	ITest test;

	@Bean(name = "car1")
	public Car get1() {
		Car car = new Car(123, "hello");
		test.m1();
		return car;
	}

	@Bean(name = "car2")
	public Car get2() {
		Car car = new Car(1234, "hello2");
		test.m1();
		return car;
	}
}
