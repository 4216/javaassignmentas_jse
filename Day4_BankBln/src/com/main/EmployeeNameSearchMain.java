package com.main;

import com.model.EmployeeModel;
import com.service.EmployeeNameService;

/**
 * 
 * @author Amani
 *
 */
 
public class EmployeeNameSearchMain {

	public static void main(String[] args) {
		EmployeeNameService service = new EmployeeNameService();
		EmployeeModel[] model = new EmployeeModel[3];
		model[0] = new EmployeeModel(101, "Amani", 2000f);
		model[1] = new EmployeeModel(102, "yamu", 3000f);
		model[2] = new EmployeeModel(103, "devarakonda", 4000f);
		String nameMatch = service.getSearchName(model, "sri");
		if (nameMatch != null) {
			System.out.println("Name is Matched :" + nameMatch);
		} else {
			System.out.println("Employee Name Not Found");
		}

	}

}
