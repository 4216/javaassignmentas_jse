package com.main;

import com.model.EmployeeModel;
import com.service.EmployeeService;

/**
 * 
 * @author Amani
 *
 */
 
//Ambiguity problem in constructor
//create two employee and let know who is drawing higher salary

public class MainClass {

	public static void main(String[] args) {
		float salary1 = 100f;
		float salary2 = 200f;
		EmployeeModel employeeModel1 = new EmployeeModel(101, "Amani", 1000f);
		EmployeeModel employeeModel2 = new EmployeeModel(102, "yamu", 2000);

		EmployeeService employeeService1 = new EmployeeService();
		EmployeeModel model1 = employeeService1.drawSalary(employeeModel1, salary1);
		EmployeeModel model2 = employeeService1.drawSalary(employeeModel2, salary2);
		if (salary1 < salary2 && model2 != null) {
			System.out.println("EMPLOYEE2 IS DRAW MAX AMOUNT");
			System.out.println("Available balance is :" + model2.getEmpolyeeSalary() + " \n" + model2);
		} else if (salary2 < salary1 && model1 != null) {
			System.out.println("EMPLOYEE1 IS DRAW MAX AMOUNT");
			System.out.println("Available balance is :" + model1.getEmpolyeeSalary() + " \n" + model1);
		}

		else {
			System.out.println("NO THERE THAT MUCH OF AMOUNT");
		}
	}

}
