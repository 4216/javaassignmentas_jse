package com.main;

import com.model.EmployeeModel;
import com.service.MutliEmpSalaryService;

public class MultipleEmployeesSalaryMain {

	public static void main(String[] args) {
		EmployeeModel employeeModel[] = new EmployeeModel[3];
		MutliEmpSalaryService service = new MutliEmpSalaryService();

		employeeModel[0] = new EmployeeModel(101, "Amani", 2000f);
		employeeModel[1] = new EmployeeModel(102, "yamu", 3000f);
		employeeModel[2] = new EmployeeModel(103, "devarakonda", 4000f);
		int salary = service.totalSalary(employeeModel);
		System.out.println("Total salary :" + salary);
	}
}
