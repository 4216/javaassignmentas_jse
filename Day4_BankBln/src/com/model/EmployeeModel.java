package com.model;

public class EmployeeModel {

	private int empolyeeId;
	private String empolyeeName;
	private float empolyeeSalary;

	public EmployeeModel() {
		super();
	}

	public EmployeeModel(int empolyeeId, String empolyeeName, float empolyeeSalary) {
		super();
		this.empolyeeId = empolyeeId;
		this.empolyeeName = empolyeeName;
		this.empolyeeSalary = empolyeeSalary;
	}

	public int getEmpolyeeId() {
		return empolyeeId;
	}

	public void setEmpolyeeId(int empolyeeId) {
		this.empolyeeId = empolyeeId;
	}

	public String getEmpolyeeName() {
		return empolyeeName;
	}

	public void setEmpolyeeName(String empolyeeName) {
		this.empolyeeName = empolyeeName;
	}

	public float getEmpolyeeSalary() {
		return empolyeeSalary;
	}

	public void setEmpolyeeSalary(float empolyeeSalary) {
		this.empolyeeSalary = empolyeeSalary;
	}

	@Override
	public String toString() {
		return "EmployeeModel [empolyeeId=" + empolyeeId + ", empolyeeName=" + empolyeeName + ", empolyeeSalary="
				+ empolyeeSalary + "]";
	}
}
