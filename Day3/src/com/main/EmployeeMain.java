package com.main;

import com.service.EmployeeService;

/**
 * 
 * @author Amani
 *
 */
public class EmployeeMain {

	public static void main(String[] args) {
		EmployeeService employeeService = new EmployeeService();
		employeeService .setData(101, "Amani", (byte) 20, 20000f);

		System.out.println(employeeService .getData());
	}

}
