package com.service;

import com.model.Employee;

public class EmployeeService {
	Employee employee = new Employee();

	public void setData(int id, String name, byte age, float salary) {
		employee.setId(id);
		employee.setName(name);
		employee.setAge(age);
		employee.setSalary(salary);

	}

	public Employee getData() {
		return employee;
	}
}
