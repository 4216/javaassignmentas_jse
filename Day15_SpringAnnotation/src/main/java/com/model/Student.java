package com.model;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

//<bean id="", class="className">
@Component()
@PropertySource("classpath:application.properties")
public class Student {

	@Value("${sNo}")
	private int sNo;

	@Value("Amani")
	private String sName;

	@Autowired
	private Address address;

	@PostConstruct
	public void start() {
		System.out.println("Student.start()");
	}

	@PreDestroy
	public void stop() {
		System.out.println("Student.stop()");

	}

	public int getsNo() {
		return sNo;
	}

	public void setsNo(int sNo) {
		this.sNo = sNo;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Student(int sNo, String sName, Address address) {
		super();
		this.sNo = sNo;
		this.sName = sName;
		this.address = address;
	}

	public Student() {
		super();
		
	}
}
