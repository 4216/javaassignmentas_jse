package com.model;

/**
 * 
 */
 
import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;




@Component
@PropertySource("classpath:application.properties")
public class Address implements Serializable {
	/**
	 * The serialVersionUID attribute is an identifier that is used to
	 * serialize/deserialize an object of a Serializable class.
	 */
	private static final long serialVersionUID = 7207449881961255244L;

	@Value("${no}")
	private Integer doorNO;
	@Value("${city}")
	private String city;
	@Value("${state}")
	private String state;
	public Address() {
		super();
		
	}
	public Address(Integer doorNO, String city, String state) {
		super();
		this.doorNO = doorNO;
		this.city = city;
		this.state = state;
	}
	public Integer getDoorNO() {
		return doorNO;
	}
	public void setDoorNO(Integer doorNO) {
		this.doorNO = doorNO;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
