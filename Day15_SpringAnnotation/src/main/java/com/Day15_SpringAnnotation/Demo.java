package com.Day15_SpringAnnotation;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.model" })
public class Demo {

//	@Bean("name")
//	public Student std() {
//		return new Student();
//	}

}
