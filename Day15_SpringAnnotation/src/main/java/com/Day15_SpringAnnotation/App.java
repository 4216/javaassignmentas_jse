package com.Day15_SpringAnnotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.AbstractApplicationContext;

import com.model.Student;

public class App {
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(Demo.class);
		Student s = context.getBean(Student.class);
//		Student s = context.getBean("name",Student.class);

		System.out.println(s.getsNo());
		((AbstractApplicationContext) context).close();
	}
}
