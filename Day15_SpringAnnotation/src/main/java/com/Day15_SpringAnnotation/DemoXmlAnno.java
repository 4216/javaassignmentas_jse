package com.Day15_SpringAnnotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Student;

public class DemoXmlAnno {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/Amani/config/applicationContext.xml");
		Student s = context.getBean("student", Student.class);
//		Student s = context.getBean(Student.class);
		System.out.print(s.getsNo());
		System.out.println(s.getsNo());
		System.out.println(s);
		((AbstractApplicationContext) context).close();
	}
}
