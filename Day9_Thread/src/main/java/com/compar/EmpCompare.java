package com.compar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



class Emp {
	public Emp(int i, String string, float f) {
		
	}
	@SuppressWarnings("unused")
	private int id;
	@SuppressWarnings("unused")
	private String name;
	@SuppressWarnings("unused")
	private float salary;
	public String getName() {
		
		return null;
	}
	public int getId() {
		
		return 0;
	}
}
	
class EmpId implements Comparator<Emp> {

	public int compare(Emp o1, Emp o2) {
		if (o1.getId() < o2.getId())
			return -1;
		if (o1.getId() > o2.getId())
			return 1;
		else
			return 0;
	}
}

class EmpName implements Comparator<Emp> {

	public int compare(Emp o1, Emp o2) {
		return o1.getName().compareTo(o2.getName());
	}



	

	public static void main(String[] args) {
		ArrayList<Emp> ar = new ArrayList<Emp>();
		ar.add(new Emp(111, "bbbb", 1100f));
		ar.add(new Emp(131, "aaaa", 900f));
		ar.add(new Emp(121, "cccc", 1300f));

		System.out.println("Unsorted");
		for (int i = 0; i < ar.size(); i++)
			System.out.println(ar.get(i));

		Collections.sort(ar, new EmpId());

		System.out.println("\nSorted by rollno");
		for (int i = 0; i < ar.size(); i++)
			System.out.println(ar.get(i));

		Collections.sort(ar, new EmpName());

		System.out.println("\nSorted by name");
		for (int i = 0; i < ar.size(); i++)
			System.out.println(ar.get(i));
	}




	public EmpName() {
		super();
		
	}
	
	

}
