package com.thread;

class Emp implements Runnable {
	@Override
	public void run() {
		System.out.println("interface Thread to call");

	}

}

public class RunnableInterface {

	public static void main(String[] args) {
		Emp emp = new Emp();
		Thread t = new Thread(emp);
		t.start();
	}

}
