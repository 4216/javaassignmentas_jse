package com.thread;

/**
 * 
 * @author Amani
 *
 */
 
 /*
 * It makes java memory efficient because garbage collector removes the
 * unreferenced objects from heap memory.
 * 
 * It is automatically done by the garbage collector(a part of JVM) so we don't
 * need to make extra efforts.
 * 
 * How can an object be unreferenced? There are many ways:
 * 
 * By nulling the reference,obj=null; By assigning a reference to
 * another,obj1=obj2 By anonymous object etc, new Employee();
 * 
 * finalize() method The finalize() method is invoked each time before the
 * object is garbage collected. This method can be used to perform cleanup
 * processing. This method is defined in Object class as:protected void
 * finalize(){}
 * 
 * gc() method The gc() method is used to invoke the garbage collector to
 * perform cleanup processing. The gc() is found in System and Runtime classes.
 */
public class GarbageClass {
	public void finalize() {
		System.out.println("object is garbage collected");
	}

	public static void main(String args[]) {
		GarbageClass s1 = new GarbageClass();
		GarbageClass s2 = new GarbageClass();
		System.out.println(s1 + "," + s2);
		s1 = null;
		s2 = null;
//		s1=s2;
		System.gc();
	}

}
