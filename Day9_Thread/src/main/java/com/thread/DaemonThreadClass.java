package com.thread;

public class DaemonThreadClass extends Thread {
	public void run() {
		if (Thread.currentThread().isDaemon()) {// checking for daemon thread
			System.out.println("daemon thread work");
		} else {
			System.out.println("user thread work");
		}
	}

	public static void main(String[] args) {
		DaemonThreadClass daemonThreadClass = new DaemonThreadClass();// creating thread
		DaemonThreadClass daemonThreadClass2 = new DaemonThreadClass();// creating thread
		DaemonThreadClass daemonThreadClass3 = new DaemonThreadClass();// creating thread
		// daemonThreadClass.start(); //setDaemon(true) after throw the exception
		daemonThreadClass.setDaemon(true);// now t1 is daemon thread

		daemonThreadClass.start();
		daemonThreadClass2.start();
		daemonThreadClass3.start();
	}

}
