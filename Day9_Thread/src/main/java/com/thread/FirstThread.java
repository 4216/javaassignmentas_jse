package com.thread;

public class FirstThread extends Thread {
	public void run() {
		System.out.println("thread class method");
		for (int i = 1; i <= 5; i++) {
			try {
				Thread.sleep(500);
			} catch (Exception e) {
				System.out.println(e);
			}
			System.out.println(i);
		}
	}

	public static void main(String[] args) {
		System.out.println("starts");
		FirstThread firstThread = new FirstThread();

		FirstThread firstThread2 = new FirstThread();
		FirstThread firstThread3 = new FirstThread();

		firstThread.setPriority(10);// min 1 norm 5 max 10
		firstThread.setName("amani");
		firstThread2.setName("amani");

		firstThread.start();
		// firstThread.start(); give a Exception in thread "main"
		// java.lang.IllegalThreadStateException

		System.out.println(firstThread.getPriority() + "," + firstThread.getName() + "," + firstThread.getId());

		try {
			firstThread.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
		firstThread2.start();
		firstThread3.start();

		System.out.println(firstThread2.getPriority() + "," + firstThread2.getName() + "," + firstThread2.getId());
	}

}
