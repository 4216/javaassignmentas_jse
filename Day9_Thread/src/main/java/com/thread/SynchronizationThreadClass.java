package com.thread;

/**
 * 
 * @author Amani
 *
 */
 
/*
 * Synchronization in Java is the capability to control the access of multiple
 * threads to any shared resource.
 * 
 * Java Synchronization is better option where we want to allow only one thread
 * to access the shared resource.
 * 
 * Thread Synchronization: There are two types of thread synchronization mutual
 * exclusive and inter-thread communication.
 * 
 * 1.Mutual Exclusive: 1.Synchronized method. 2.Synchronized block(this/obj ref). 3.Static(.class name)
 * synchronization. 2.Cooperation (Inter-thread communication in java)
 */
class Table {
	synchronized void printTable(int n) {// method not synchronized
		for (int i = 1; i <= 5; i++) {
			System.out.println(n * i);
			try {
				Thread.sleep(400);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}
}

class MyThread1 extends Thread {
	Table t;

	MyThread1(Table t) {
		this.t = t;
	}

	public void run() {
		t.printTable(5);
	}
}

class MyThread2 extends Thread {
	Table t;

	MyThread2(Table t) {
		this.t = t;
	}

	public void run() {
		t.printTable(100);
	}
}

public class SynchronizationThreadClass {

	public static void main(String[] args) {
		Table table = new Table();
		MyThread1 myThread1 = new MyThread1(table);
		MyThread2 myThread2 = new MyThread2(table);
		myThread1.start();
		myThread2.start();

	}

}
