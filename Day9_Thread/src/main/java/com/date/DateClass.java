package com.date;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public class DateClass {

	public static void main(String[] args) {
		Date date = new Date();
		System.out.println(date);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = dateFormat.format(date);
		System.out.println(strDate);
		Calendar c = Calendar.getInstance();
		System.out.println("The Current Date is:" + c.getTime());

		LocalDate date1 = LocalDate.now();
		LocalDate yesterday = date1.minusDays(1);
		LocalDate tomorrow = date1.plusDays(1);
		System.out.println("Today date: " + date1);
		System.out.println("Yesterday date: " + yesterday);
		System.out.println("Tomorrow date: " + tomorrow);
	}
}
