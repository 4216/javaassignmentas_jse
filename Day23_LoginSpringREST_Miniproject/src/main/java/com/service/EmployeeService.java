package com.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeService implements IEmployeeService {

	@Override
	public Employee insertEmp(Employee employee) {
		boolean flag = validCheck(employee);
		Employee emp = null;
		if (flag) {
			emp = employee;
		} else {
			emp = null;
		}
		return emp;
	}

	@Override
	public Employee readEmp(int id, String name) {
		Employee employee = new Employee();
		employee.setId(id);
		employee.setName(name);

		List<Employee> list = new ArrayList<Employee>();
		list.add(new Employee(101, "Amani", "hyd", 20));
		list.add(new Employee(102, "sweety", "bng", 21));
		list.add(new Employee(103, "devarakonda", "chennai", 22));
		list.add(new Employee(104, "female", "hyd", 23));

		Employee emp = null;
		boolean flag = validCheck(employee);
		if (flag) {
			for (Employee employee2 : list) {
				if (employee2.getId() == employee.getId() && employee.getName().equalsIgnoreCase(employee2.getName())) {
					emp = employee2;
					return emp;
				}
			}
		} else {
			return null;
		}
		return emp;
	}

	@Override
	public Employee updateEmp(Employee employee) {
		boolean flag = validCheck(employee);
		Employee emp = null;
		if (flag) {
			emp = employee;
		} else {
			emp = null;
		}
		return emp;
	}

	@Override
	public int delete(int id) {
		return id;
	}

	@Override
	public boolean validCheck(Employee employee) {
		boolean flag = false;
		String id = String.valueOf(employee.getId());
		if (id.length() >= 3 && employee.getName().length() >= 3) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}
}
