package com.service;

import com.model.Employee;

public interface IEmployeeService {
	public boolean validCheck(Employee employee);

	public Employee insertEmp(Employee employee);

	public Employee updateEmp(Employee employee);

	public Employee readEmp(int id, String name);

	public int delete(int emp);
}
