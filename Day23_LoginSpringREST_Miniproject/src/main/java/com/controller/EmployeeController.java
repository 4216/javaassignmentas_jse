package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.model.Employee;
import com.service.IEmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	IEmployeeService service;

	@PostMapping("/insert")
	public ResponseEntity<Employee> insertEmp(@RequestBody Employee employee) {
		Employee emp = service.insertEmp(employee);
		System.out.println(emp);
		ResponseEntity<Employee> res = new ResponseEntity<Employee>(emp, HttpStatus.CREATED);
		return res;
	}

	@GetMapping("/get")
//	http://localhost:8080/Day23_LoginSpringREST_SM/get/101/srikanth
//	public ResponseEntity<Employee> getData(@PathVariable int id, @PathVariable String name) {
//	http://localhost:8080/Day23_LoginSpringREST_SM/get?id=101&name=srikanth
	public ResponseEntity<Employee> getData(@RequestParam("id") int id, @RequestParam("name") String name) {

		Employee employee = service.readEmp(id, name);
		ResponseEntity<Employee> res = null;
		if (employee != null) {
			res = new ResponseEntity<Employee>(employee, HttpStatus.FOUND);
		} else {
			System.out.println("please Enter a valid data...");
		}
		return res;
	}

//	{"name":"amani","location":"abc","age":212,"id":101}
	@PutMapping("/update")
	public ResponseEntity<Employee> updateEmployee(@RequestBody Employee employee) {
		Employee emp = service.updateEmp(employee);
		ResponseEntity<Employee> res = null;
		if (emp != null) {
			res = new ResponseEntity<Employee>(emp, HttpStatus.OK);
		} else {
			System.out.println("ENTER A VALID DETAILS");
		}
		return res;
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Boolean> deleteEmployee(@PathVariable int id) {
		int delete = service.delete(id);
		System.out.println("deleted employee No : " + delete);
		ResponseEntity<Boolean> resEntity = new ResponseEntity<Boolean>(true, HttpStatus.GONE);
		return resEntity;
	}

}
