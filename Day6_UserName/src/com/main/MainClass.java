package com.main;



import java.util.Scanner;


import com.model.User;
import com.service.IUserService;
import com.serviceImpl.UserService;

public class MainClass {

	public static void main(String[] args) {

		IUserService service = new UserService();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter User Id And PassWord");
		Integer id = sc.nextInt();
		String password = sc.next();
		User model = new User();
		model.setId(id);
		model.setPassword(password);
		User result2 = service.checkUserIdAndPassword(model);

		try {
			if (result2.equals(null)) {
				System.out.println("Please Enter Your Correct User ID and Password...");
			} else {
				System.out.println("Welcome Mr: " + result2.getUserName());
			}
		} catch (Exception e) {
			System.out.println("Please Enter Your Correct User ID and Password...");
		}

		finally {
			try {
				sc.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				model = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				service = null;
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
}
