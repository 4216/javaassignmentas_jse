package com.service;

import com.model.User;

/**
 * 
 * @author Amani
 *
 */

public interface IUserService {
	public String validUserIdAndPassword(Integer id, String password);

	public User checkUserIdAndPassword(User model);
}
