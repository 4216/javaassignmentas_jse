package com.dao;

import java.util.List;

import org.hibernate.Session;

import com.model.Student;

public class StudentDao implements IStudentDao {
	public Session session;

	public StudentDao(Session session) {
		this.session = session;
	}

	@Override
	public Student insert(Student student) {
		session.save(student);// insert
		return student;
	}

	@Override
	public Student update(Student student) {
		Student a = session.find(Student.class, student.getsNo());
		if (a != null) {
			session.clear();
			session.update(student);
			a = student;
		} else {
			a = null;
		}
		return a;
	}

	@Override
	public void delete(int id) {
		Student student = new Student();
		student.setsNo(id);
		session.delete(student);
		System.out.println("TRANCTION IS DELETED");
	}

	@Override
	public Student retrive(int id) {
		Student student = null;
		student = session.load(Student.class, id);// object not found exception

//		String readAllEmployee = "From Student";
//		List<Student> s = session.createQuery(readAllEmployee).list();
//		System.out.println(s);
		return student;
	}

}
