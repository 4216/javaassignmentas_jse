package com.dao;

import com.model.Student;

public interface IStudentDao {
	public Student insert(Student student);

	public Student update(Student student);

	public void delete(int id);

	public Student retrive(int id);

}
