package com.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.dao.IStudentDao;
import com.dao.StudentDao;
import com.model.Student;
import com.util.HibernateConnection;

public class StudentService implements IStudentService {
	SessionFactory factory = HibernateConnection.getSessionFactory();
	Session session = factory.openSession();
	Transaction transaction;
	IStudentDao dao = new StudentDao(session);

	@Override
	public Student insert(Student student) {
		boolean flag = validCheck(student);
		Student s = null;
		if (flag) {
			transaction = session.beginTransaction();
			s = dao.insert(student);
			transaction.commit();
		}
		return s;
	}

	@Override
	public Student update(Student student) {
		boolean flag = validCheck(student);
		Student s = null;
		if (flag) {
			transaction = session.beginTransaction();
			s = dao.update(student);
			transaction.commit();
		}
		return s;
	}

	@Override
	public void delete(int id) {
		Student student = new Student();
		student.setsNo(id);
		boolean flag = validCheck(student);
		if (flag) {
			session.clear();
			transaction = session.beginTransaction();
			dao.delete(id);
			transaction.commit();
		}
	}

	@Override
	public Student retrive(int id) {
		Student student = new Student();
		Student s = null;
		student.setsNo(id);
		boolean flag = validCheck(student);
		if (flag) {
			s = dao.retrive(id);
		}
		return s;
	}

	@Override
	public boolean validCheck(Student student) {
		boolean flag = false;
		String id = String.valueOf(student.getsNo());
		if (student.getsNo() >= 1 && id.length() >= 1) {
			System.out.println("StudentService.validCheck()");
			flag = true;
			if (student.getsName() != null && student.getsName().length() >= 3) {
				flag = true;
			}
		}
		return flag;
	}
}
