package com.service;

import com.model.Student;

public interface IStudentService {
	public Student insert(Student student);

	public Student update(Student student);

	public void delete(int id);

	public Student retrive(int id);

	public boolean validCheck(Student student);

}
