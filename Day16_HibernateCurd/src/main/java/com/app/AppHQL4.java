package com.app;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Student;

public class AppHQL4 {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/sri/config/hibernate.cfg.xml")
				.build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		// HQL

		String hqlRead = "from Student";// "select *From table_name";
		Query<Student> query = session.createQuery(hqlRead);// JPA ==result set
		List<Student> studentList = query.list();
		for (Student student : studentList) {
			System.out.println(student.getsNo() + "," + student.getsName() + ", " + student.getMark());
		}

		String hqlRead1 = "select s.sName from Student as s";// "select *From table_name";
		org.hibernate.query.Query<String> query1 = session.createQuery(hqlRead1);
		List<String> student1 = query1.list();
		for (String student : student1) {
			System.out.println(student);
		}

		// Named Params(=:abc) and start with from only don't use select
		String hqlRead2 = "from Student where sNo=:abc";
		org.hibernate.query.Query<Student> query2 = session.createQuery(hqlRead2);
		query2.setInteger("abc", 1);// (postion , value);
		List<Student> student2 = query2.list();
		for (Student student : student2) {
			System.out.println(student);
		}

		String hqlRead3 = "from Student where sNo>0 ORDER BY sNo DESC";
		Query<Student> query3 = session.createQuery(hqlRead3);// JPA ==result set
		List<Student> studentList3 = query3.list();
		for (Student student11 : studentList3) {
			System.out.println(student11.getsNo() + "," + student11.getsName() + ", " + student11.getMark());
		}

		System.out.println("End of Hibernate");
	}
}
