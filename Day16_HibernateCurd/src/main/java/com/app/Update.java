package com.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Student;

public class Update {
	public static void main(String[] args) {

		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/sri/config/hibernate.cfg.xml")
				.build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		Student student = new Student();
		Transaction transaction = session.beginTransaction();
		Student a = session.find(Student.class, 3);
		if (a != null) {
			session.clear();
			student.setsNo(3);
			student.setsName("Sri1");
			student.setMark(70);
			session.update(student);
//			session.saveOrUpdate(student);
			System.out.println("TRANCTION IS updated ");

		} else {
			System.out.println("Id is not find");
		}

		transaction.commit();
	}
}
