package com.app;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Student;

import javassist.tools.rmi.ObjectNotFoundException;

/**
 * Hello world!
 *
 */
public class AppLoad {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("com/sri/config/hibernate.cfg.xml")
				.build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		Student student = session.load(Student.class, 3);// object not found exception
		try {
			System.out.println(student.getsNo());
			System.out.println(student.getsName());
			System.out.println(student.getMark());
		} catch (org.hibernate.ObjectNotFoundException e) {
			System.out.println("ObjectNotFoundException");
		}
		System.out.println("End of Hibernate");

	}
}
