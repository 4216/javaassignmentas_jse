package com.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Vechile;
import com.service.IDemoService;

/**
 * 
 * @author srikanth_penta
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/sri/config/applicationContext.xml");
		IDemoService service = context.getBean("sri", IDemoService.class);
		Vechile vechile = service.demo();
		System.out.println(vechile);
		((AbstractApplicationContext) context).close();
	}
}
