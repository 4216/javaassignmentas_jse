package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.model.Vechile;

@Component("Amani")
public class DemoService implements IDemoService {

	@Autowired
	Vechile first;

	public Vechile demo() {
		first.setNumber(132);
		first.setEnginNumber("12Elh");
		first.setVachileType("bus...To");
		return first;

	}
}
