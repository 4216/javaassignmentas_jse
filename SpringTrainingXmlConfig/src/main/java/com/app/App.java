package com.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.service.DemoService;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("com/sri/config/config.xml");
		DemoService service = ctx.getBean("sri", DemoService.class);
		System.out.println(service.demo());
	}
}
