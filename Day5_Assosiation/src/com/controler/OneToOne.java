package com.controler;

import com.model.Department;
import com.model.Employee;
import com.service.IOneToOneService;
import com.serviceImpl.OneToOneService;

/**
 * 
 * @author Amani
 *
 */

public class OneToOne {

	public static void main(String[] args) {
		// one to one
		System.out.println("One to One");
		Department department = new Department(1, "Software Engineer");
		Employee employee1 = new Employee(101, "Srikanth", department);
		IOneToOneService service = new OneToOneService();
		employee1 = service.getData(employee1);
		if (employee1 != null) {
			System.out.println(employee1);
		}

		// Many to one
		System.out.println("Many to one");
		Employee employee2 = new Employee(102, "Amani", department);
		employee2 = service.getData(employee2);
		if (employee1 != null && employee2 != null) {
			System.out.println(employee1 + "\n" + employee2);
		}

		// one to many

		System.out.println("One to Many");
		Department department3 = new Department(2, "Software Tester");
		Employee employee3 = new Employee(102, "Sri", department3);
		employee3 = service.getData(employee3);
		if (employee3 != null && employee2 != null) {
			System.out.println(employee2 + "\n" + employee3);
		}
	}

	/*
	 * 5) Deparment + Employee a) Employee[ ] Address [ ] b) Address [ ]
	 * 
	 * 6) Organisation Department[ ] Employee [ ] Address [ ]
	 * 
	 * HR -- ? HDCF project --> developer -- > in Bangalore
	 * 
	 * HCL (iterate ) Department (iterate) Collection Of Employee address.getCity()
	 * .equals("Bangalore");
	 * 
	 * Hr == "Name of the person" who stays in Bangalore
	 */

}
