package com.model;

import java.io.Serializable;

/**
 * 
 * @author Amani
 *
 */



public class Employee implements Serializable {
	
	private static final long serialVersionUID = 8325171485438216917L;

	
	private Integer id;
	private String name;
	private Department department;
	public Employee() {
		super();
		
	}
	public Employee(Integer id, String name, Department department) {
		super();
		this.id = id;
		this.name = name;
		this.department = department;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", department=" + department + "]";
	}
	
	
	
}
