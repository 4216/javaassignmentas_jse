package com.service;

import com.model.Employee;

/**
 * 
 * @author Amani
 *
 */

public interface IOneToOneService {
	Employee getData(Employee employee);
}
