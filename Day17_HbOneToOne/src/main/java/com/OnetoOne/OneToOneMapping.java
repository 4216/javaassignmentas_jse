package com.OnetoOne;

import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.model.Address;
import com.model.Employee;
import com.util.HibernateConnection;

/**
 * 
 * @author Amani
 *
 */

public class OneToOneMapping {
//	static Transaction tx;
	public static void main(String[] args) throws HibernateException {

		Session session = HibernateConnection.getSessionFactory().getCurrentSession();
		try {
			Transaction tx = session.beginTransaction();

			/*
			 * Employee employee = new Employee(); employee.setName("Amani");
			 * employee.setEmail("amani@gmail.com");
			 * 
			 * Address address = new Address(); address.setCity("Ghaziabad");
			 * address.setState("UP"); address.setCountry("India");
			 * address.setPincode(201301);
			 * 
			 * employee.setAddress(address); address.setEmployee(employee);
			 * session.save(employee);
			 */

			/*
			 * TypedQuery query = session.createQuery("from Employee"); List<Employee> list
			 * = query.getResultList();
			 * 
			 * Iterator<Employee> itr = list.iterator(); while (itr.hasNext()) { Employee
			 * emp = itr.next(); System.out.println(emp.getEmployeeId() + " " +
			 * emp.getName() + " " + emp.getEmail()); Address address = emp.getAddress();
			 * System.out.println(address.getCity() + " " + address.getState() + " " +
			 * address.getCountry() + " " + address.getPincode()); }
			 */

			TypedQuery query = session.createQuery("from Address");
			List<Address> list = query.getResultList();

			Iterator<Address> itr = list.iterator();
			while (itr.hasNext()) {
				Address address = itr.next();
				System.out.println(address.getCity() + " " + address.getState() + " " + address.getCountry() + " "
						+ address.getPincode());
				Employee emp = address.getEmployee();
				System.out.println(emp.getEmployeeId() + " " + emp.getName() + " " + emp.getEmail());
			}

			
			tx.commit();
			HibernateConnection.shutdown();// close the connection
			System.out.println("closed the connection");
		} catch (Exception e) {
//			tx.rollback();
			System.out.println("error...");
		}
	}

}
