package com.dao;

import com.model.User;

public interface IUserDao {
	public User createUser();

	public User insertUser(User user);

	public User updateUser(User user);

	public User deleteUser(User user);

	public User userValidation(User user);

}
