package com.service;

import com.exception.UserExceptionValidation;
import com.model.User;

public interface IUserService {
	public User createUser();

	public User insertUser(User user)throws UserExceptionValidation;

	public User updateUser(User user);

	public User deleteUser(User user);

	User userValidation(User user);
}
