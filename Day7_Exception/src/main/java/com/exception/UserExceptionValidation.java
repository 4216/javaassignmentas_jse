package com.exception;
/**
 * 
 * @author srikanth_penta
 *
 */
//Exception is catch the exception in compile time check 
//RuntimeException is catch the exception in runtime

public class UserExceptionValidation extends Exception {
	private static final long serialVersionUID = -7910586993420521726L;

	public UserExceptionValidation(String errorMessage) {
		super(errorMessage);
	}

}
