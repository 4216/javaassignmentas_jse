package com.serviceImpl;

import com.dao.IUserDao;
import com.daoImpl.UserDaoImpl;
import com.exception.UserExceptionValidation;
import com.model.User;
import com.service.IUserService;

public class UserServiceImpl implements IUserService {
	IUserDao dao = new UserDaoImpl();

	@Override
	public User createUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User insertUser(User user)throws UserExceptionValidation {
		boolean flag = validCheck(user);
		User user1 = null;
		if (flag) {
			user1 = user;
		} else {
			user1 = null;
//			throw new UserExceptionValidation("");
		}
		return user1;
	}

	@Override
	public User updateUser(User user) {
		boolean flag = validCheck(user);
		User user1 = null;
		if (flag) {
			user1 = dao.updateUser(user);
		} else {
			user1 = null;
		}
		return user1;
	}

	@Override
	public User deleteUser(User user) {
		boolean flag = validCheck(user);
		User user1 = null;
		if (flag) {
			user1 = dao.deleteUser(user);
			if (user1 != null) {
				user1 = null;
			} else {
				user1 = user;
			}
		} else {
			user1 = user;
		}
		return user1;
	}

	@Override
	public User userValidation(User user) {
	User user2=dao.userValidation(user);
		return user2;
	}

	public boolean validCheck(User user) {
		String id = String.valueOf(user.getId());
		boolean flag = false;
		if (id.length() >= 6 && user.getPassword().length() >= 6) {
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

}
