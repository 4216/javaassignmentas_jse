package com.app;

import java.util.Scanner;

import com.exception.UserExceptionValidation;
import com.model.User;
import com.service.IUserService;
import com.serviceImpl.UserServiceImpl;

/**
 * Hello world!
 *
 */
/*
 * try{} is a block to write the risky code catch(Ex e){} is a method to handle
 * the exception finally {} is a block to clean up activity and execute always
 * throw new EX(); is use to generate the exception throws EX -> forward the
 * exception and this work method level
 */
public class App {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		IUserService service = new UserServiceImpl();
		Scanner sc = new Scanner(System.in);

		// validation
		try {
			User userValid = service.userValidation(new User(987654, "abcxyz"));
			if (userValid != null) {
				System.out.println(userValid);
			} else {
				throw new UserExceptionValidation("Enter the Correct Details...");
			}
		} catch (UserExceptionValidation | NullPointerException e) {
			e.printStackTrace();
		}
		// insert
		try {
			System.out.println("Enter The Inserted Details...s");
			Integer id = sc.nextInt();
			String name = sc.next();
			String password = sc.next();
			User userIs = service.insertUser(new User(id, name, password));
			if (userIs != null) {
				System.out.println("Inserted " + userIs);
			} else {
				throw new UserExceptionValidation("Enter the Correct Details...");
			}
		} catch (UserExceptionValidation e) {
			e.printStackTrace();
		}

		// update
		try {
			System.out.println("Enter The Updated User Details...");
			Integer id = sc.nextInt();
			String name = sc.next();
			String password = sc.next();
			User userUp = service.updateUser(new User(id, name, password));
			if (userUp != null) {
				System.out.println("Updated the user Details " + userUp);
			} else {
				throw new UserExceptionValidation("Enter the Correct Details...");
			}
		} catch (UserExceptionValidation e) {
			e.printStackTrace();
		}

		// delete
		try {
			System.out.println("Enter The Delete User Details...");
			Integer id = sc.nextInt();
			String password = sc.next();
			User userValid = service.deleteUser(new User(id, password));
			if (userValid == null) {
				System.out.println(id + " User Is deleted successfully..");
			} else {
				throw new UserExceptionValidation("Enter the Correct Details...");
			}
		} catch (UserExceptionValidation | NullPointerException e) {
			e.printStackTrace();
		}

		finally {
			sc.close();
			service = null;

		}
	}
}
