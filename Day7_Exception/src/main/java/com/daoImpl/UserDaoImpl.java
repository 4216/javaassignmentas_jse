package com.daoImpl;

import com.dao.IUserDao;
import com.model.User;

public class UserDaoImpl implements IUserDao {

	@Override
	public User createUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User insertUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User updateUser(User user) {
		User user1 = null;
		if (user.getId() == 123456) {
			user1 = user;
		} else if (user.getId() == 987654) {
			user1 = user;
		} else {
			user1 = null;
		}
		return user1;
	}

	@Override
	public User deleteUser(User user) {
		User user1 = null;
		if (user.getId() == 123456) {
			user1 = user;
		} else if (user.getId() == 987654) {
			user1 = user;
		} else {
			user1 = null;
		}
		return user1;
	}

	@Override
	public User userValidation(User user) {
		String id = String.valueOf(user.getId());
		if (id.length() >= 6 && user.getPassword().length() >= 6) {
			if (user.getId() == 123456 && user.getPassword().equals("abcdef")) {
				user.setName("Srikanth");
			} else if (user.getId() == 987654 && user.getPassword().equals("abcxyz")) {
				user.setName("Sri");
			}
		} else {
			user = null;
		}
		return user;
	}

}
