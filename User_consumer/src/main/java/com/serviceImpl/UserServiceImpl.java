package com.serviceImpl;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.exception.UserNotFoundException;
import com.model.PetDto;
import com.model.User;
import com.repo.UserRepo;
import com.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Amani
 *
 */
 
@Service
@Slf4j
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepo userRepo;
	
//	@Autowired
//	private PetRepo petRepo;

	User user = null;

	@Override
	@Transactional
	public User addUser(User user1) {
		String name = user1.getUserName();
		String password = user1.getUserPassword();
		String confimPassword = user1.getConfirmPassword();

		if (name.equals(password)) {
			log.info("username and password must not be same");
			throw new UserNotFoundException("username and password must not be same");
		} else {
			if (password.equals(confimPassword)) {

				user = new User();
				user.setUserName(user1.getUserName());
				user.setUserPassword(user1.getUserPassword());
				user.setConfirmPassword(user1.getConfirmPassword());
				Set<PetDto> listPet = new HashSet<>();
				for (PetDto pet : user1.getPets()) {
					PetDto pp = new PetDto();
					pp.setName(pet.getName());
					pp.setAge(pet.getAge());
					pp.setPlace(pet.getPlace());
					pp.setOwner(user);
					listPet.add(pp);
				}
				user.setPets(listPet);

				User customer = userRepo.save(user);
				return customer;
			} else {
				log.info("Password do no match");
				throw new UserNotFoundException("Password do no match");
			}
		}
	}

	@Override
	@Transactional
	public User updateUser(User user1) {
		Optional<User> userFind = userRepo.findById((Long) user1.getId());
		User userUpdate = null;
		if (userFind != null) {
			User user = new User();
			user.setUserName(user1.getUserName());
			user.setUserPassword(user1.getUserPassword());
			user.setConfirmPassword(user1.getConfirmPassword());
			Set<PetDto> listPet = new HashSet<>();
			for (PetDto pet : user1.getPets()) {
				PetDto pp = new PetDto();
				pp.setName(pet.getName());
				pp.setAge(pet.getAge());
				pp.setPlace(pet.getPlace());
				pp.setOwner(user);
				listPet.add(pp);
			}
			user.setPets(listPet);

			userUpdate = userRepo.save(user);
		} else {
			userUpdate = null;
		}
		return userUpdate;
	}

	@Override
	@Transactional
	public List<User> listUsers() {
		return userRepo.findAll();
	}

	@Override
	@Transactional
	public User getUserById(Long id) {
		Optional<User> user = userRepo.findById(id);
		return user.get();
	}

	@Override
	@Transactional
	public String removeUser(Long id) {
		Optional<User> userFind = userRepo.findById(id);
		String info = null;
		if (userFind != null) {
			userRepo.deleteById(id);
			info = "user is deleted successfully...";
		} else {
			info = "user is not found";
		}
		return info;
	}

	@Override
	@Transactional
	public User findByUserName(String name) {
		return userRepo.findByUserName(name);
	}

	@Override
	@Transactional
	public PetDto buyPet(PetDto pet) {
		/*
		 * Optional<Pet> pet1 = petRepo.findById(pet.getId()); Pet pet2 = pet1.get();
		 * Pet pet3 = petRepo.save(pet2);
		 */
//		petRepo.deleteById(pet.getId());
//		return pet3;
		return null;
	}

	@Override
	@Transactional
	public Optional<PetDto> getMyPets(Long id) {
//		Optional<PetDto> pet = petRepo.findById(id);
//		return pet;
		return null;
	}

	@Override
	@Transactional
	public User loginUser(String name, String password, String confimPassword) {
		User user = null;
		if (name != password) {
			if (password.equals(confimPassword)) {
				user = userRepo.findByUserNameAndUserPassword(name, password);
			} else {
				log.info("password and confimpassword should be same");
				throw new UserNotFoundException("password and confimpassword should be same");
			}
			return user;
		} else {
			log.info("username and password must not be same");
			throw new UserNotFoundException("username and password must not be same");
		}
	}

}
