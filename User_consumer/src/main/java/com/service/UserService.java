package com.service;

import java.util.List;
import java.util.Optional;

import com.model.PetDto;
import com.model.User;

/**
 * 
 * @author Amani
 *
 */
 
public interface UserService {
	public abstract User addUser(User user);

	public abstract User updateUser(User use);

	public abstract List<User> listUsers();

	public abstract User getUserById(Long id);

	public abstract String removeUser(Long id);

	public abstract User findByUserName(String name);

	public abstract PetDto buyPet(PetDto pet);

	public abstract Optional<PetDto> getMyPets(Long id);

	public abstract User loginUser(String name, String password, String confimPassword);
}
