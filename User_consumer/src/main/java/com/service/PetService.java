package com.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.model.PetDto;

/**
 * 
 * @author Amani
 *
 */

@FeignClient(name = "petProducer", url = "http://localhost:9876/pets")
public interface PetService {

	@GetMapping("/say")
	public abstract ResponseEntity<String> sayHello();

	@GetMapping("/")
	public abstract List<PetDto> petHome();

	@GetMapping("/myPets")
	public abstract ResponseEntity<List<PetDto>> myPets();

	@GetMapping("/petDetail")
	public abstract ResponseEntity<List<PetDto>> petDetails();

	@PostMapping("/addPet")
	public abstract ResponseEntity<PetDto> addPet(@RequestBody PetDto pet);

	@PutMapping("/buyPet/{petId}")
	public abstract PetDto buyPet(@PathVariable Long petId);
}
