package com.validator;

import java.io.Serializable;
import java.util.Set;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.model.PetDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Amani
 *
 */
 
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class UserValidator implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * @NotNull(groups = AdvanceInfo.class)
	 * @Valid is use to method level with the type of class and variable
	 * @Validated(AdvanceInfo.class) is a class level annotation
	 * 
	 */

	@NotEmpty(message = "Name may not be empty")
	@Size(min = 2, max = 55, message = "Name must be between 2 and 55 characters long")
	private String userName;

	@NotEmpty(message = "Password may not be empty")
	@Size(min = 2, max = 55, message = "password must be between 2 and 55 characters long")
	private String userPassword;

	private transient String confirmPassword;

	private Set<PetDto> pets;
}
