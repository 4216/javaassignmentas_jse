package com.validator;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Amani
 *
 */
 

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Component
public class PetValidator implements Serializable {

	private static final long serialVersionUID = -6499781759965842383L;

	@NotEmpty(message = "Name may not be empty")
	@Size(min = 2, max = 55, message = "Name must be between 2 and 32 characters long")
	private String name;

//	@Digits(fraction = 0, integer = 0, message = "No decimals allowed")
	@Min(value = 2, message = "must be equal or greater than 2")
	@Max(value = 50, message = "must be equal or less than 50")
	private Integer age;

}
