package com.model;

import java.io.Serializable;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Amani
 *
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PetDto implements Serializable {
	private static final long serialVersionUID = 5915548474243144780L;

	private Long id;

	@NotEmpty(message = "Name may not be empty")
	@Size(min = 2, max = 55, message = "Name must be between 2 and 55 characters long")
	private String name;

	@Min(value = 2, message = "must be equal or greater than 2")
	@Max(value = 50, message = "must be equal or less than 50")
	private Integer age;

	@Size(min = 2, max = 55, message = "PET_PLACE must be between 2 and 55 characters long")
	private String place;

	private User owner;
}
