package com.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Amani
 *
 */
 
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PET_USER1")
public class User implements Serializable {

	private static final long serialVersionUID = -5934010972169898359L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private Long id;

	
	@Column(name = "USER_NAME", length = 55, unique = true, nullable = true)
	private String userName;

	
	@Column(name = "USER_PASSWD", length = 55, nullable = false)
	private String userPassword;

	private transient String confirmPassword;

//	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
//	@JsonIgnoreProperties({ "owner" })
//	@ManyToOne
	private Set<PetDto> pets;
	
}
