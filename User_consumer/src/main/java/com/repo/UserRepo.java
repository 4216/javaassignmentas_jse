package com.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.User;

/**
 * 
 * @author Amani
 *
 */

public interface UserRepo extends JpaRepository<User, Long> {
	public User findByUserName(String name);

	public User findByUserNameAndUserPassword(String name, String password);
}
