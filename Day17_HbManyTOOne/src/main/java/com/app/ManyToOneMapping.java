package com.app;

import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.model.Address;
import com.model.Employee;
import com.util.HibernateConnection;

/**
 * 
 * @author Amani
 *
 */
 
public class ManyToOneMapping {

	public static void main(String[] args) throws HibernateException {

		Session session = HibernateConnection.getSessionFactory().getCurrentSession();
		try {
			Transaction tx = session.beginTransaction();

			/*
			 * Employee employee1 = new Employee(); employee1.setName("Amani");
			 * employee1.setEmail("amani@gmail.com");
			 * 
			 * Employee employee2 = new Employee(); employee2.setName("ammu");
			 * employee2.setEmail("ammu@gmail.com");
			 * 
			 * Address address = new Address(); address.setCity("Noida");
			 * address.setState("UP"); address.setCountry("India");
			 * address.setPincode(201301);
			 * 
			 * employee1.setAddress(address); employee2.setAddress(address);
			 * 
			 * session.save(employee1); session.save(employee2);
			 */

			TypedQuery query = session.createQuery("from Employee");
			List<Employee> list = query.getResultList();

			Iterator<Employee> itr = list.iterator();
			while (itr.hasNext()) {
				Employee emp = itr.next();
				System.out.println(emp.getEmployeeId() + " " + emp.getName() + " " + emp.getEmail());
				Address address = emp.getAddress();
				System.out.println(address.getCity() + " " + address.getState() + " " + address.getCountry() + " "
						+ address.getPincode());
			}

			tx.commit();
			HibernateConnection.shutdown();// close the connection
			System.out.println("closed the connection");
		} catch (Exception e) {
//			tx.rollback();
			System.out.println("error...");
		}
	}

}
