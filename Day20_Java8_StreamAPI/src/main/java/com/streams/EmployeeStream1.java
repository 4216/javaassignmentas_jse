package com.streams;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class EmployeeStream1 {

	public static void main(String[] args) {
		Employee employee1 = new Employee(101, "Ten", 1010.0f, 'A');
		Employee employee2 = new Employee(102, "Twenty", 2020.20f, 'B');
		Employee employee3 = new Employee(103, "Thirty", 3030.30f, 'A');
		Employee employee4 = new Employee(104, "Fourty", 2525.0f, 'D');

		ArrayList<Employee> list = new ArrayList<>();
		list.add(employee1);
		list.add(employee2);
		list.add(employee3);
		list.add(employee4);

		long data = list.stream().count();
		System.out.println("Number OF  Records  " + data);

		// Iteration
		list.stream().forEach((abc) -> {
			System.out.println(abc.getEmpName());
		});
//		forEach-> Consumer

		list.stream().limit(2).forEach(a -> {
			System.out.println(a);
		});

		System.out.println("FIND FIRST  " + list.stream().findFirst().get());
		System.out.println("FIND Any  " + list.stream().findAny().get());

//		Employee e = list.stream().filter(a -> a.getEmpName().equalsIgnoreCase("T"));
//		System.out.println("FIND AllMatch  " +e );

		// Intermediate
//		list.stream().filter((arg) -> arg.getEmpName().startsWith("F")).count();
//		list.stream().filter((arg) -> arg.getEmpName().startsWith("F")).collect(Collectors.toList());;
		long howMany = list.stream().filter((arg) -> arg.getEmpName().startsWith("T")).count();
		System.out.println("How Many names Starts With  'F' :" + howMany);
//		filter->predicate return true or false

		list.stream().filter((arg) -> arg.getEmpName().startsWith("T")).forEach((getData) -> {
			System.out.println(getData.getEmpNo() + " " + getData.getEmpName() + " " + getData.getSalary());
		});

		long result = list.stream().filter((arg) -> arg.getBand().equals('A')).map((m) -> m.getSalary() + 500).count();
		System.out.println("Employee in band \"a\" and have a hike of " + result);

		long result1 = list.stream().filter((arg) -> arg.getBand().equals('A')).filter((ip) -> ip.getEmpNo() == 1010.0f)
				.map((m) -> m.getSalary() + 500).count();
		System.out.println(result1);
	}
}
