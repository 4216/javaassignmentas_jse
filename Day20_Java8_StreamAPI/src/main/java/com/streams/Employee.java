package com.streams;

import java.io.Serializable;

public class Employee implements Serializable {
	private static final long serialVersionUID = -5717405289763304042L;

	private Integer empNo;
	private String empName;
	private Float salary;
	private Character band;

	public Integer getEmpNo() {
		return empNo;
	}

	public void setEmpNo(Integer empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Float getSalary() {
		return salary;
	}

	public void setSalary(Float salary) {
		this.salary = salary;
	}

	public Character getBand() {
		return band;
	}

	public void setBand(Character band) {
		this.band = band;
	}

	public Employee(Integer empNo, String empName, Float salary, char band) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.band = band;
	}

	public Employee() {
		super();
	}

	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + ", empName=" + empName + ", salary=" + salary + ", band=" + band + "]";
	}
}
