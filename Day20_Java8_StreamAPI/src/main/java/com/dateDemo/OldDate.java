package com.dateDemo;

import java.time.LocalDate;

public class OldDate {

	public static void main(String[] args) {
		System.out.println(new java.util.Date());

		LocalDate date = LocalDate.of(2021, 10, 21);
		System.out.println(date);
	}
}
