package com.hcl.pp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.pp.model.Pet;
/**
 * 
 * @author Amani
 *
 */
 
public interface PetRepo extends JpaRepository<Pet, Long> {

}
