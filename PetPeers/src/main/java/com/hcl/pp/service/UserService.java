package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;

/**
 * 
 * @author Amani
 *
 */
 
public interface UserService {
	public abstract User addUser(User user);

	public abstract User updateUser(User use);

	public abstract List<User> listUsers();

	public abstract User getUserById(Long id);

	public abstract String removeUser(Long id);

	public abstract User findByUserName(String name);

	public abstract Pet buyPet(Pet pet);

	public abstract Optional<Pet> getMyPets(Long id);

	public abstract User loginUser(String name, String password, String confimPassword);
}
