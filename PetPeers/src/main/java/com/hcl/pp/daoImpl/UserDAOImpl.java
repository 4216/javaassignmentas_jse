package com.hcl.pp.daoImpl;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hcl.pp.dao.UserDAO;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.repo.PetRepo;
import com.hcl.pp.repo.UserRepo;

/**
 * 
 * @author Amani
 *
 */
 
//@Component
@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private PetRepo petRepo;

	@Override
	public User addUser(User user1) {
		User user = new User();
		user.setUserName(user1.getUserName());
		user.setUserPassword(user1.getUserPassword());
		user.setConfirmPassword(user1.getConfirmPassword());
		Set<Pet> listPet = new HashSet<>();
		for (Pet pet : user1.getPets()) {
			Pet pp = new Pet();
			pp.setName(pet.getName());
			pp.setAge(pet.getAge());
			pp.setPlace(pet.getPlace());
			pp.setOwner(user);
			listPet.add(pp);
		}
		user.setPets(listPet);

		User customer = userRepo.save(user);
		return customer;
	}

	@Override
	public User updateUser(User user1) {
		Optional<User> userFind = userRepo.findById((Long) user1.getId());
		User userUpdate = null;
		if (userFind != null) {
			User user = new User();
			user.setUserName(user1.getUserName());
			user.setUserPassword(user1.getUserPassword());
			user.setConfirmPassword(user1.getConfirmPassword());
			Set<Pet> listPet = new HashSet<>();
			for (Pet pet : user1.getPets()) {
				Pet pp = new Pet();
				pp.setName(pet.getName());
				pp.setAge(pet.getAge());
				pp.setPlace(pet.getPlace());
				pp.setOwner(user);
				listPet.add(pp);
			}
			user.setPets(listPet);

			userUpdate = userRepo.save(user);
		} else {
			userUpdate = null;
		}
		return userUpdate;
	}

	@Override
	public List<User> listUsers() {
		return userRepo.findAll();
	}

	@Override
	public Optional<User> getUserById(Long id) {
		return userRepo.findById(id);
	}

	@Override
	public User findByUserName(String name) {
		return userRepo.findByUserName(name);
	}

	@Override
	public String removeUser(Long id) {
		Optional<User> userFind = userRepo.findById(id);
		String info = null;
		if (userFind != null) {
			userRepo.deleteById(id);
			info = "user is deleted successfully...";
		} else {
			info = "user is not found";
		}
		return info;
	}

	@Override
	public User authenticateUser(String name, String password) {
		User getUser = userRepo.findByUserNameAndUserPassword(name, password);
		return getUser;
	}

	@Override
	public Pet buyPet(Pet pet) {
		Optional<Pet> pet1 = petRepo.findById(pet.getId());
		Pet pet2 = pet1.get();
		Pet pet3 = petRepo.save(pet2);
		petRepo.deleteById(pet.getId());
		return pet3;
	}

	@Override
	public Optional<Pet> getMyPets(Long id) {
		Optional<Pet> pet = petRepo.findById(id);
		return pet;
	}

}
