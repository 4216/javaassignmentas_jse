package com.hcl.pp.daoImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pp.dao.PetDAO;
import com.hcl.pp.model.Pet;
import com.hcl.pp.repo.PetRepo;
/**
 * 
 * @author Amani
 *
 */
 
//@Component
@Repository
public class PetDAOImpl implements PetDAO {

	@Autowired
	private PetRepo petRepo;

	@Override
	@Transactional
	public Pet getPetById(Long id) {
		Optional<Pet> pet = petRepo.findById(id);
		return pet.get();
	}

	@Override
	@Transactional
	public Pet savePet(Pet pet) {
		return petRepo.save(pet);
	}

	@Override
	@Transactional
	public List<Pet> fetchAll() {
		return petRepo.findAll();
	}

}
