package com.hcl.pp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Amani
 *
 */
 
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PET_USER")
public class User implements Serializable {

	private static final long serialVersionUID = -5934010972169898359L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private Long id;

	@NotEmpty(message = "Name may not be empty")
	@Size(min = 2, max = 55, message = "Name must be between 2 and 55 characters long")
	@Column(name = "USER_NAME", length = 55, unique = true, nullable = true)
	private String userName;

	@NotEmpty(message = "Password may not be empty")
	@Size(min = 2, max = 55, message = "password must be between 2 and 55 characters long")
	@Column(name = "USER_PASSWD", length = 55, nullable = false)
	private String userPassword;

	private transient String confirmPassword;

	@OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
	@JsonIgnoreProperties({ "owner" })
	private Set<Pet> pets;
}
