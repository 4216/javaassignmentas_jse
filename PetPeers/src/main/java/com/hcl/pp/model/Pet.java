package com.hcl.pp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author Amani
 *
 */
 
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PETS")
public class Pet implements Serializable {
	private static final long serialVersionUID = 5915548474243144780L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", length = 5)
	private Long id;

	@NotEmpty(message = "Name may not be empty")
	@Size(min = 2, max = 55, message = "Name must be between 2 and 55 characters long")
	@Column(name = "PET_NAME", length = 55)
	private String name;

//	@Digits(integer = 1, fraction = 99)
//	@Size(message = "Age should be 0 and 99 years")
  @Min(value=18, message="must be equal or greater than 18")  
    @Max(value=45, message="must be equal or less than 45")  
	@Column(name = "PET_AGE", length = 2, nullable = true)
	private Integer age;

	@Size(min = 2, max = 55, message = "PET_PLACE must be between 2 and 55 characters long")
	@Column(name = "PET_PLACE", length = 55)
	private String place;

	@JoinColumn(name = "PET_OWNERID")
	@ManyToOne(fetch = FetchType.EAGER)
	private User owner;
}
