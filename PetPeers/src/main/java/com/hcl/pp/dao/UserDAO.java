package com.hcl.pp.dao;

import java.util.List;
import java.util.Optional;

import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
/**
 * 
 * @author Amani
 *
 */
 
public interface UserDAO {
	public abstract User addUser(User user);

	public abstract User updateUser(User user);

	public abstract List<User> listUsers();

	public abstract Optional<User> getUserById(Long id);

	public abstract User findByUserName(String name);

	public abstract String removeUser(Long id);

	public abstract User authenticateUser(String name, String password);

	public abstract Pet buyPet(Pet pet);

	public abstract Optional<Pet> getMyPets(Long id);
}
