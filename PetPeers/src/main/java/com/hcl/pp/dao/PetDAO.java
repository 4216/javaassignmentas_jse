package com.hcl.pp.dao;

import java.util.List;

import com.hcl.pp.model.Pet;
/**
 * 
 * @author Amani
 *
 */

public interface PetDAO {
	public abstract Pet getPetById(Long id);

	public abstract Pet savePet(Pet pet);

	public abstract List<Pet> fetchAll();
}