package com.hcl.pp.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.PetsNotFoundException;
import com.hcl.pp.exception.UserNotFoundException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.PetValidator;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Amani
 *
 */
 
@RestController
@Slf4j
@Validated
@RequestMapping("/pets")
public class PersAppController {

	@Autowired
	private PetService petService;

	@Autowired
	private UserService userService;

	PetValidator petValidation;

	@GetMapping("/")
	public List<Pet> petHome() {
		log.info("PersAppController.petHome()");
		return petService.getAllPets();
	}

	@GetMapping("/myPets")
	public ResponseEntity<List<Pet>> myPets() {
		List<Pet> pet = null;
		try {
			log.info("PersAppController.myPets()");
			pet = petService.fetchAll();
			return new ResponseEntity<List<Pet>>(pet, HttpStatus.FOUND);
		} catch (Exception e) {
			return new ResponseEntity<List<Pet>>(pet, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/petDetail")
	public ResponseEntity<List<Pet>> petDetails() {
		log.info("PersAppController.petDetails()");
		List<Pet> pet = petService.getAllPets();
		return new ResponseEntity<List<Pet>>(pet, HttpStatus.FOUND);
	}

	@PostMapping("/addPet")
	public ResponseEntity<Pet> addPet(@RequestBody Pet pet) {
		try {
			log.info("PersAppController.addPet()");
			Pet pet1 = petService.savePet(pet);
			return new ResponseEntity<Pet>(pet1, HttpStatus.CREATED);
		} catch (PetsNotFoundException e) {
			throw new PetsNotFoundException("pease enter correct data");
		}
	}

	@PutMapping("/buyPet/{petId}")
	public ResponseEntity<Pet> buyPet(@PathVariable Long petId) {
		try {
			log.info("PersAppController.buyPet()");
			Pet petI = new Pet();
			petI.setId(petId);
			Pet pet = userService.buyPet(petI);
			return new ResponseEntity<Pet>(pet, HttpStatus.FOUND);
		} catch (PetsNotFoundException e) {
			throw new PetsNotFoundException("pease enter correct Id");
		}
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ConstraintViolationException.class)
	public Map<String, String> handleConstraintViolation(ConstraintViolationException ex) {
		log.info("UserController.handleConstraintViolation()");
		Map<String, String> errors = new HashMap<>();
		ex.getConstraintViolations().forEach(cv -> {
			errors.put("message", cv.getMessage());
			errors.put("path", (cv.getPropertyPath()).toString());
		});
		return errors;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(DataIntegrityViolationException.class)
	public Map<String, String> dataIntegrityViolationException(HttpServletRequest req,
			DataIntegrityViolationException ex) {
		log.info("User Name is already used. please select  a different User Name");
		Map<String, String> errors = new HashMap<>();
		errors.put("message", "User Name is already used. please select  a different User Name");
		errors.put("path", req.getContextPath().toString());
		return errors;
	}

	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<Object> exception(UserNotFoundException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.CREATED);
	}

	@ExceptionHandler(value = NoSuchElementException.class)
	public ResponseEntity<Object> valueNotPresent(NoSuchElementException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.CREATED);
	}
}
