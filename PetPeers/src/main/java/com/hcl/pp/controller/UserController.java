package com.hcl.pp.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.exception.UserNotFoundException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.PetService;
import com.hcl.pp.service.UserService;
import com.hcl.pp.validator.LoginValidator;
import com.hcl.pp.validator.PetValidator;
import com.hcl.pp.validator.UserValidator;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Amani
 *
 */
 
@RestController
@Slf4j
@RequestMapping("/user")
@Validated
public class UserController {
	@Autowired
	private PetService petService;

	@Autowired
	private UserService userService;

	UserValidator userValidation;
	LoginValidator loginValidation;
	PetValidator petValidation;

	@PostMapping("/add")
	public ResponseEntity<User> addUser(@RequestBody @Valid User user) {
		User user1 = null;
		if (user != null) {
			userValidation = new UserValidator();
			petValidation = new PetValidator();

			userValidation.setUserName(user.getUserName());
			userValidation.setUserPassword(user.getUserPassword());
			userValidation.setConfirmPassword(user.getConfirmPassword());
			Set<Pet> pet = user.getPets();
			for (Pet pet1 : pet) {
				petValidation.setName(pet1.getName());
				petValidation.setAge(pet1.getAge());
			}

			String name = userValidation.getUserName();
			String password = userValidation.getUserPassword();
			String confimPassword = userValidation.getConfirmPassword();

			if (name.equalsIgnoreCase(password)) {
				log.info("username and password must not be same");
				throw new UserNotFoundException("username and password must not be same");
			} else {
				if (password.equals(confimPassword)) {
					user1 = userService.addUser(user);
					return new ResponseEntity<User>(user1, HttpStatus.CREATED);
				} else {
					log.info("Password do no match");
					throw new UserNotFoundException("Password do no match");
				}
			}
		} else {
			throw new UserNotFoundException("please enter");
		}
	}

	@GetMapping("/login")
	public ResponseEntity<Object> loginUser(
			@RequestParam("name") @Valid @Size(min = 3, max = 55, message = "Name must be between 3 and 55 characters long") String name1,
			@RequestParam("password") @Valid @Size(min = 3, max = 55, message = "password must be between 3 and 55 characters long") String password1,
			@RequestParam("confimPassword") @Valid @Size(min = 3, max = 55, message = "password and confirmPassword Not Matched") String confimPassword1) {

		loginValidation = new LoginValidator();
		loginValidation.setUserName(name1);
		loginValidation.setUserPassword(password1);
		loginValidation.setConfirmPassword(confimPassword1);

		String name = loginValidation.getUserName();
		String password = loginValidation.getUserPassword();
		String confimPassword = loginValidation.getConfirmPassword();
		List<Pet> pet = null;
		User user = null;
		if (name.equals(password)) {
			log.info("username and password must not be same");
			throw new UserNotFoundException("username and password must not be same");
		} else {
			if (password.equals(confimPassword)) {
				user = userService.loginUser(name, password, confimPassword);
				if (user != null) {
					pet = petService.getAllPets();
					return new ResponseEntity<>(pet, HttpStatus.FOUND);
				}
			} else {
				log.info("password and confimPassword should be same");
				throw new UserNotFoundException("password and confimpassword should be same");
			}
		}
		return new ResponseEntity<>(user, HttpStatus.FOUND);
	}

	@GetMapping("/logout")
	public ResponseEntity<String> logout() {
		log.info("PersAppController.logout()");
		return new ResponseEntity<String>("please login...", HttpStatus.ACCEPTED);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(ConstraintViolationException.class)
	public Map<String, String> handleConstraintViolation(ConstraintViolationException ex) {
		log.info("UserController.handleConstraintViolation()");
		Map<String, String> errors = new HashMap<>();
		ex.getConstraintViolations().forEach(cv -> {
			errors.put("message", cv.getMessage());
			errors.put("path", (cv.getPropertyPath()).toString());
		});
		return errors;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(DataIntegrityViolationException.class)
	public Map<String, String> dataIntegrityViolationException(HttpServletRequest req,
			DataIntegrityViolationException ex) {
		log.info("User Name is already used. please select  a different User Name");
		Map<String, String> errors = new HashMap<>();
		errors.put("message", "User Name is already used. please select  a different User Name");
		errors.put("path", req.getContextPath().toString());
		return errors;
	}

	@ExceptionHandler(value = UserNotFoundException.class)
	public ResponseEntity<Object> exception(UserNotFoundException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.CREATED);
	}

	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	public ResponseEntity<Object> badRequest(MethodArgumentNotValidException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
	}

//	@ExceptionHandler(value = DefaultMessageSourceResolvable.class)
	public ResponseEntity<Object> badRe(DefaultMessageSourceResolvable exception) {
		return new ResponseEntity<>(exception.getDefaultMessage(), HttpStatus.BAD_REQUEST);
	}
}
