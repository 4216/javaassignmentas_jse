package com.hcl.pp.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pp.dao.UserDAO;
import com.hcl.pp.exception.UserNotFoundException;
import com.hcl.pp.model.Pet;
import com.hcl.pp.model.User;
import com.hcl.pp.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Amani
 *
 */

@Service
@Slf4j
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDAO userDao;

	@Override
	@Transactional
	public User addUser(User user) {
		User user1 = null;
		String name = user.getUserName();
		String password = user.getUserPassword();
		String confimPassword = user.getConfirmPassword();

		if (name.equals(password)) {
			log.info("username and password must not be same");
			throw new UserNotFoundException("username and password must not be same");
		} else {
			if (password.equals(confimPassword)) {
				user1 = userDao.addUser(user);
			} else {
				log.info("Password do no match");
				throw new UserNotFoundException("Password do no match");
			}
			return user1;
		}
	}

	@Override
	@Transactional
	public User updateUser(User user) {
		User user1 = userDao.updateUser(user);
		return user1;
	}

	@Override
	@Transactional
	public List<User> listUsers() {
		return userDao.listUsers();
	}

	@Override
	@Transactional
	public User getUserById(Long id) {
		Optional<User> user = userDao.getUserById(id);
		return user.get();
	}

	@Override
	@Transactional
	public String removeUser(Long id) {
		return userDao.removeUser(id);
	}

	@Override
	@Transactional
	public User findByUserName(String name) {
		return userDao.findByUserName(name);
	}

	@Override
	@Transactional
	public Pet buyPet(Pet pet) {
		return userDao.buyPet(pet);
	}

	@Override
	@Transactional
	public Optional<Pet> getMyPets(Long id) {
		return userDao.getMyPets(id);
	}

	@Override
	@Transactional
	public User loginUser(String name, String password, String confimPassword) {
		User user = null;
		if (name != password) {
			if (password.equals(confimPassword)) {
				user = userDao.authenticateUser(name, password);
			} else {
				log.info("password and confimpassword should be same");
				throw new UserNotFoundException("password and confimpassword should be same");
			}
			return user;
		} else {
			log.info("username and password must not be same");
			throw new UserNotFoundException("username and password must not be same");
		}
	}

}
