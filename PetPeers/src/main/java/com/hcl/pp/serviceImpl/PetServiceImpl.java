package com.hcl.pp.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.pp.dao.PetDAO;
import com.hcl.pp.model.Pet;
import com.hcl.pp.service.PetService;

/**
 * 
 * @author Amani
 *
 */
 
@Service
public class PetServiceImpl implements PetService {

	@Autowired
	private PetDAO petDao;

	@Override
	@Transactional
	public Pet savePet(Pet pet) {
		return petDao.savePet(pet);
	}

	@Override
	@Transactional
	public List<Pet> getAllPets() {
		return petDao.fetchAll();
	}

	@Override
	@Transactional
	public List<Pet> fetchAll() {
		return petDao.fetchAll();
	}

}
