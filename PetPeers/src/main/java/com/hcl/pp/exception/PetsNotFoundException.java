package com.hcl.pp.exception;
/**
 * 
 * @author Amani
 *
 */
 
public class PetsNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -8924870519415522525L;
	private String massage;

	public PetsNotFoundException() {
		super();
	}

	public PetsNotFoundException(String massage) {
		super(String.format(" '%s'", massage));
		this.massage = massage;
	}

	public String getMassage() {
		return massage;
	}

}
