package com.hcl.pp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * 
 * @author Amani
 *
 */
 
@ControllerAdvice
public class PetsExceptionController extends ResponseEntityExceptionHandler{

	@ExceptionHandler(value = PetsNotFoundException.class)
	public ResponseEntity<Object> exception(PetsNotFoundException exception) {
		return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
	}
}
