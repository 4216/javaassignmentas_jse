package com.test;

/**
 * @author Amani
 */

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import com.model.Employee;
import com.service.*;

public class TestSet {

	public static void main(String[] args) {
		StringBuffer br = new StringBuffer("hello1234567890123456789");
		System.out.println(br.capacity());
		System.out.println(br.length());

		SetClass setClass = new SetClass();
		Set<Employee> emp = setClass.setEmp();
		for (Employee e : emp) {
			System.out.println(e.getId() + "," + e.getName() + " " + e.getSalary());
		}

		System.out.println("list of employees");
		ListClass listClass = new ListClass();
		List<Employee> list = listClass.getEmp();
		Iterator<Employee> itr = list.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

		System.out.println("		QueueClass of employees");
		QueueClass queueClass = new QueueClass();
		Queue<Employee> queue = queueClass.getEmpQ();
		queue.forEach(e -> System.out.println(e));

		System.out.println("MapClass...");
		MapClass mapClass = new MapClass();
		Map<Integer, Employee> map = mapClass.getMap();
		System.out.println(map);
	}
}
