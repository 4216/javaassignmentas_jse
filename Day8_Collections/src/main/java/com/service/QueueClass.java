package com.service;

/**
 * @author Amani
 */

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

import com.model.Employee;

public class QueueClass {
	Queue<Employee> queue;
	Queue<Employee> queue1;

	Queue<Employee> setEmpQ() {
		queue = new PriorityQueue<>();
		queue1 = new PriorityQueue<>();

		queue.add(new Employee(101, "Queue", 700f));
		queue1.addAll(queue);

		queue = new LinkedList<>();// java 11 version
		queue.add(new Employee(102, "LinkedList", 800f));
//		queue1.addAll(queue);

		Deque<Employee> deque = new ArrayDeque<>();
		deque.add(new Employee(102, "LinkedList", 800f));
//		queue1.addAll(deque);
		return queue1;
	}

	public Queue<Employee> getEmpQ() {
		return setEmpQ();
	}
}
