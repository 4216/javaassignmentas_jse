package com.service;

/**
 * @author Amani
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

import com.model.Employee;

public class ListClass {
	List<Employee> list;
	List<Employee> l;

	List<Employee> setEmp() {
		list = new ArrayList<>();
		l = new ArrayList<>();
		list.add(new Employee(101, "Amani", 200f));
		list.add(new Employee(102, "ammu", 300f));
		l.addAll(list);

		list = new LinkedList<>();
		list.add(new Employee(103, "sweety", 400f));
		list.add(new Employee(104, "amsweety", 500f));
		l.addAll(list);

		list = new Vector<>();// introduced java 1.1-> thread safe
		list.add(new Employee(105, "vactor", 600f));
		l.addAll(list);

		list = new Stack<>();
		list.add(new Employee(106, "Stack", 700f));
		l.addAll(list);

		return l;
	}

	public List<Employee> getEmp() {
		return setEmp();
	}
}
