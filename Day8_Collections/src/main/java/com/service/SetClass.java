package com.service;

/**
 * @author Amani
 */

import java.util.HashSet;
import java.util.Set;

import com.model.Employee;

public class SetClass {
	public Set<Employee> setEmp() {
		Set<Employee> s = new HashSet<>();
		s.add(new Employee(101, "hell1", 1200f));
		s.add(new Employee(102, "hell2", 1300f));
		s.add(new Employee(103, "hell3", 1400f));
		s.add(new Employee(104, "hell4", 1500f));
//		Iterator<Employee> it = s.iterator();

		Set<Employee> emp = new HashSet<>();
		// while (it.hasNext()) {
		// for (Employee e : s) {
		// System.out.println(it.next().getId());
		emp.addAll(s);
		// System.out.println(e.getName());
		// }
		return emp;
	}
}
