package com.service;

/**
 * @author Amani
 */

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.model.Employee;

public class MapClass {

	Map<Integer, Employee> map;
	Map<Integer, Employee> map1;

	Map<Integer, Employee> setMap() {
		map = new HashMap<>();
		map.put(101, new Employee(101, "amani", 1200f));
		// map1.putAll(map);

		map = new Hashtable<>();
		map.put(102, new Employee(102, "ammu", 1100f));
		// map1.putAll(map);

		return map;
	}

	public Map<Integer, Employee> getMap() {
		return setMap();
	}
}
