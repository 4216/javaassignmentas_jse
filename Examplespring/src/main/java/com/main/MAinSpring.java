package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Employee;

public class MAinSpring {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/spring.xml");
		Employee employee= (Employee) applicationContext.getBean("employeeId");
		
		      System.out.println("Spring"+employee);
		      
		      System.out.println("Employee Number"+ employee.getEmpNo());
		      System.out.println("Employee Name"+ employee.getEmpName());
		      
		      employee.setEmpNo(123456);
		      employee.setEmpName("Amani");
		      employee.setSalary(1.2f);
		      
		      System.out.println("Employee Number"+ employee.getEmpNo());
		      System.out.println("Employee Name"+ employee.getEmpName());
		      System.out.println("Employee Salary"+ employee.getSalary());
		      
		      
		      
		      
		      Employee employee2 = new Employee();
		      System.out.println(employee2);//heap memory address hexa decimal 
		      System.out.println("Employee Number"+ employee2.getEmpNo());
		      System.out.println("Employee Name"+ employee2.getEmpName());
              System.out.println("The End");
              
              
              
	}
}