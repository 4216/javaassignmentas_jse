package com.main;
/**
 * 
 * @author srikanth_penta
 *
 */
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.dao.UserDao;
import com.model.User;

public class Read {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo1_persist_xml");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		// invoke the dao
		UserDao dao = new UserDao(entityManager); // constructor injection

		List<User> lu = dao.findAll();
		if (lu.isEmpty()) {
			System.out.println("sorry not there data...");
		} else {
			for (User user : lu) {
				System.out.println(user.getId());
				System.out.println(user.getName());
			}
		}

		Optional<User> ou = dao.findById(1);
		if (ou.isPresent()) {
			User user1 = ou.get();
			System.out.println(user1.getId());
			System.out.println(user1.getName());
		}

//		update
		User user2 = new User();
		user2.setId(1);
		user2.setName("patel");
		Optional<User> update = dao.update(user2);
		if (update.isPresent()) {
			System.out.println(update.get().getName());
		}

//		delete
		Integer i = dao.deleteById(1);
		if (i != null) {
			System.out.println("deleted ...");
		} else {
			System.out.println("Not Found the id");
		}

	}
}
