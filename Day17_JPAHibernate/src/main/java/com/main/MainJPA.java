package com.main;

/**
 * 
 * @author srikanth_penta
 *
 */
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.dao.UserDao;
import com.model.User;

public class MainJPA {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo1_persist_xml");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		// invoke the dao
		UserDao dao = new UserDao(entityManager); // constructor injection
		User user = new User();
//		user.setId(new Integer(12));//this is primary key
		user.setName("world");

		Optional<User> optional = dao.save(user);
		if (optional.isPresent()) {
			System.out.println(optional.get().getId());
			System.out.println(optional.get().getName());
		} else {
			System.out.println("sorry not there data...");
		}

	}

}
