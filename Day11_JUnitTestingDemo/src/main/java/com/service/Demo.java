package com.service;

public class Demo {
	public String show() {
		return "welcome";
	}

	public String getName(String arg) {
		String dummy = null;
		if (arg.length() > 5) {
			dummy = "welcome" + arg;
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}
		return dummy;
	}
}
