package com.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.service.Demo;

@SuppressWarnings("unused")
public class DemoTest {

	@Test
//	@Ignore
	public void showTest() {
		Demo demo = new Demo();
		String actual = demo.show();
		String expected = "welcome";
		assertEquals(actual, expected);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void testGetName() {
		Demo demo = new Demo();
		assertEquals("welcomeHCL", demo.getName("HCL"));
	}
}
