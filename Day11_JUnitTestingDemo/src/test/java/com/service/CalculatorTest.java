package com.service;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.service.Calculator;

@SuppressWarnings("unused")
public class CalculatorTest {
	static Calculator calculator;
	Calculator calculator2;

	@Before
	public void startTest2() {
		calculator2 = new Calculator();
		System.out.println("CalculatorTest.startTest2()");
	}

	@BeforeClass
	public static void startTest() {
		calculator = new Calculator();
		System.out.println("CalculatorTest.startTest()");
	}

	@Test
	public void testAdd() {
		assertEquals(30, calculator.add(10, 20));
	}

	@Test
	public void testAdd1() {
		int ans = calculator.add(1, 2);
		assertEquals(3, ans);
	}

	@AfterClass
	public static void stopTest() {
		calculator = null;
		System.out.println("CalculatorTest.stopTest()");
	}

	@Test
	public void testAdd2() {
		int ans = calculator2.add(1, 2);
		assertEquals(3, ans);
	}

	@After
	public void stopTest2() {
		calculator2 = null;
		System.out.println("CalculatorTest.stopTest2()");
	}
}
