package com.app;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.model.Student;
import com.util.HibernateUtil;

public class App {
	public static void main(String[] args) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session ses = HibernateUtil.getSession();
		try {
			// Load object (ses.get(-,-))
			Student student = ses.get(Student.class, 1);
			if (student == null) {
				System.out.println("record not found");
			} else {
				System.out.println("record not found and it is" + student);
			}
		} catch (HibernateException he) {
			he.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// perform Tx Mgmt
			// close HB objects
			HibernateUtil.closeSession(ses);
			HibernateUtil.closeSessionFactory();
		} // finally */

	}// main
}// class
