package com.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//@Data
//@NoArgsConstructor
//@AllArgsConstructor
public class Student implements Serializable {

	private static final long serialVersionUID = 1L;// Marker Interface == no abstract method to override

	private int sNo;
	private String sName;
	private int mark;

	// whenever you work with framework create default

	public int getsNo() {
		return sNo;
	}

	public void setsNo(int sNo) {
		this.sNo = sNo;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public Student() {
		super();
	}

	public Student(int sNo, String sName, int mark) {
		super();
		this.sNo = sNo;
		this.sName = sName;
		this.mark = mark;
	}

	@Override
	public String toString() {
		return "Student [sNo=" + sNo + ", sName=" + sName + ", mark=" + mark + "]";
	}

}
