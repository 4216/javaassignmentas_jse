package com.main;

import java.util.List;

import com.model.User;
import com.service.IUserService;
import com.service.UserServiceImpl;

public class MainJPA {

	public static void main(String[] args) {

		IUserService service = new UserServiceImpl(); // constructor injection
//		User user = new User();
//		user.setName("amani");
//		Optional<User> optional = service.save(user);
//		if (optional.isPresent()) {
//			System.out.println(optional.get().getId());
//			System.out.println(optional.get().getName());
//		} else {
//		System.out.println("sorry not there data...");
//		}

//		select All the data
		List<User> lu = service.findAll();
		if (lu.isEmpty()) {
			System.out.println("sorry not there data...");
		} else {
			for (User user1 : lu) {
				System.out.println(user1.getId());
				System.out.println(user1.getName());
			}
		}

//		FindByID
//		Optional<User> ou = service.findById(2);
//		if (ou.isPresent()) {
//			User user1 = ou.get();
//			System.out.println(user1.getId());
//			System.out.println(user1.getName());
//		}

//		update
//		User user2 = new User();
//		user2.setId(2);
//		user2.setName("patel");
//		Optional<User> update = service.update(user2);
//		if (update.isPresent()) {
//			System.out.println(update.get().getName());
//		}

//		delete
//		Integer i = service.deleteById(2);
//		if (i != null) {
//			System.out.println("deleted ...");
//		} else {
//			System.out.println("Not Found the id");
//		}

	}

}
