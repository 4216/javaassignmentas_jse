package com.service;

import java.util.List;
import java.util.Optional;

import com.model.User;

public interface IUserService {
	public Optional<User> save(User user);

	public Optional<User> findById(Integer id);

	public List<User> findAll();

	public Optional<User> update(User user);

	public Integer deleteById(Integer id);

	boolean validCheck(User user);

}
