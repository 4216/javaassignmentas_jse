package com.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.dao.IUserDao;
import com.dao.UserDaoImpl;
import com.model.User;
import com.util.JPAConnection;

/**
 * 
 * @author Amani
 *
 */

public class UserServiceImpl implements IUserService {

	public EntityManager entityManager = JPAConnection.getConnection();

	IUserDao dao = new UserDaoImpl(entityManager); // constructor injection

	public Optional<User> save(User user) {
		Optional<User> optional = null;
		boolean flag = validCheck(user);
		if (flag) {
			entityManager.getTransaction().begin();
			optional = dao.save(user);
			entityManager.getTransaction().commit();
			return optional;
		} else {
			return optional;
		}
	}

	public Optional<User> findById(Integer id) {
		User user = new User();
		user.setId(id);
		boolean flag = validCheck(user);
		Optional<User> ou = null;
		if (flag) {
			ou = dao.findById(1);
			return ou;
		} else {
			return ou;
		}
	}

	public List<User> findAll() {
		List<User> lu = dao.findAll();
		return lu;
	}

	public Optional<User> update(User user) {
		boolean flag = validCheck(user);
		if (flag) {
			return dao.update(user);
		} else {
			return null;
		}
	}

	public Integer deleteById(Integer id) {
		User user = new User();
		user.setId(id);
		Integer i = null;
		boolean flag = validCheck(user);
		if (flag) {
			i = dao.deleteById(id);
		} else {
			i = null;
		}
		return i;
	}

	public boolean validCheck(User user) {
		boolean flag = false;
		if (String.valueOf(user.getId()).length() >= 1) {
			flag = true;
			if (user.getName() != null && user.getName().length() >= 3) {
				flag = true;
			}
		}
		return flag;
	}

}
