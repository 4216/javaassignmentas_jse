package com.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;




@Entity
@Table(name = "order_table")
public class Order implements Serializable {
	private static final long serialVersionUID = 8546480742668302753L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer Id;
	private String Name;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public Order(Integer id, String name) {
		super();
		Id = id;
		Name = name;
	}

	public Order() {
		super();
	}

	@Override
	public String toString() {
		return "Order [Id=" + Id + ", Name=" + Name + "]";
	}

}
