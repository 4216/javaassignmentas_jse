package com.OnetoMany;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Order;
import com.model.Product;
import com.util.HibernateConnection;

/**
 * 
 * @author Amani
 *
 */

public class OneToManyMapping {

	public static void main(String[] args) throws HibernateException {

		Session session = HibernateConnection.getSessionFactory().getCurrentSession();
		try {
			Transaction tx = session.beginTransaction();

			Order order1 = new Order(1, "flipkart");
			Order order2 = new Order(2, "amazon");
			Set<Order> set = new HashSet<>();
			set.add(order1);
			set.add(order2);

			Product product = new Product(101, "sweet", 100f, set);
			session.save(order1);
			session.save(order2);
			session.save(product);
			tx.commit();

			HibernateConnection.shutdown();// close the connection
			System.out.println("closed the connection");
		} catch (Exception e) {
			System.out.println("error...");
		}
	}

}
