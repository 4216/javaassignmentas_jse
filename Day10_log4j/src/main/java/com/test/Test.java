package com.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.service.EmployeeService;

public class Test {
	private static final Logger LOGGER = LogManager.getLogger(Test.class);

	// Define a static logger variable so that it references the
	// Logger instance named "MyApp".

//	static Logger logger = Logger.getLogger(Test.class);

	
	

	public static void main(String[] args) {
		
		// Set up a simple configuration that logs on the console.
//		BasicConfigurator.configure();
//		logger.info("Entering application.");
		
		
		
		LOGGER.info("Your in the main class");
		LOGGER.error("error");
		LOGGER.warn("warning");
		EmployeeService employeeSerice = new EmployeeService();
		int result = employeeSerice.add(4, 8);
		System.out.println(result);
		System.out.println("the end");
	}

}
