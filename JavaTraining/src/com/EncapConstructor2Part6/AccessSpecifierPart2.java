package com.EncapConstructor2Part6;

import com.EncapConstructorPart6.*;

class C extends AccessSpecifiers {
	void m1() {
		System.out.println("C.m1()");
		l1();
	}
}

public class AccessSpecifierPart2 {

	public static void main(String[] args) {
		C c = new C();
		c.m1();
	}

}
