package com.clsObjPart5;

/*
 * Java StringBuilder class is used to create mutable (modifiable) String. The Java StringBuilder class is same as StringBuffer class except that it is non-synchronized. It is available since JDK 1.5.
 */
public class StringBuilderClass {

	public static void main(String[] args) {
		StringBuilder s = new StringBuilder("ama");
		s.append("hello");
//all StringBuffer methods are apply here
		System.out.println(s);
	}

}
