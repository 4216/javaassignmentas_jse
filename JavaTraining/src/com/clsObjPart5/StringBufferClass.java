package com.clsObjPart5;

/**
 * 
 * @author Amani  Java StringBuffer class is used to create mutable (modifiable)
         String objects. The StringBuffer class in Java is the same as String
         class except it is mutable i.e. it can be changed.
 *
 */
 
 
  
public class StringBufferClass {

	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer("amani");
		System.out.println(sb);
		System.out.println(sb.replace(5, 6, "ad"));
		System.out.println(sb.reverse());
		System.out.println(sb.insert(2, "hello"));
		System.out.println(sb.delete(0, 10));
		System.out.println(sb.append("aaa"));
	}

}
