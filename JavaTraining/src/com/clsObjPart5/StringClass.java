package com.clsObjPart5;

/**
 * @author Amani
 */

/**
 * 
 * string is basically an object that represents sequence of char values. An
 * array of characters works same as Java string.
 *
 */
public class StringClass {

	String name = "amani";// litteral
	String n = new String("amani");

	void m1() {
		System.out.println(name == n + "--" + name.hashCode() + "=" + n.hashCode());// here take hashCode value, it
		// return int,The == operator compares references not values.
		System.out.println(name.equals(n));// here take equals value, it retuen boolean ,it compare the actual value

		char ch[] = new char[name.length()];
		int i = 0;
		while (name.length() > i) {
			ch[i] = name.charAt(i);
			i++;
		}
		System.out.println(ch);

		System.out.println(name.replace('n', 'p'));
		System.out.println(n.substring(2, 5));
		System.out.println(name.toLowerCase());
		System.out.println(name.toUpperCase());

		String s = "amani";
		String ss[] = s.split(" ");
		System.out.println(ss[0]);

		System.out.println(s.concat(name));
		System.out.println(s);
	}

	public static void main(String[] args) {
		StringClass c = new StringClass();
		c.m1();
		System.out.println(c.hashCode());

	}
}
