package com.clsObjPart5;

public class ClassObject {
	void m1() {
		System.out.println("first method");
	}

	void m2() {
		System.out.println("second method");
	}

	public static void main(String[] args) {
//		local variables allocate memory in stack area
		int a = 10;// stack area

//		 methods are loaded stack area in calling order
		ClassObject obj = new ClassObject();
		obj.m1();
		obj.m2();
		System.out.println(a + "," + obj + "," + obj.hashCode());
	}

}
