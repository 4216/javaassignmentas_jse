package com.clsObjPart5;

/**
 * 
 * @author Amani
 *
 */

public class ObjectLeve {

	int id = 10;
	String name = "amani";
	String address = null;// "jpr";

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjectLeve other = (ObjectLeve) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public static void main(String[] args) throws InterruptedException {
		ObjectLeve ol = new ObjectLeve();
		System.out.println(ol.hashCode());
		System.out.println(ol.equals(ol));

	}

	@Override
	public String toString() {
		return "ObjectLeve [id=" + id + ", name=" + name + ", address=" + address + "]";
	}

}
