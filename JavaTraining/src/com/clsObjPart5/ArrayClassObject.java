/**copy right 
 * reserved*/
package com.clsObjPart5;

class A {
	Integer id;
	String name;
}

public class ArrayClassObject {

	public static void main(String[] args) {
		A a1 = new A();
		a1.id = 10;
		a1.name = "amani";
		A a2 = new A();
		a2.id = 20;
		a2.name = "ammu";

		A aa[] = { a1, a2 };
		System.out.println(a1.id + "," + a1.name);
		System.out.println(a2.id + "," + a2.name);
		for (int i = 0; i < aa.length; i++) {
			System.out.println(aa[i].id + "->" + aa[i].name);
		}

		for (A a3 : aa) {
			System.out.println(a3.id + "--" + a3.name);
		}
	}

}
