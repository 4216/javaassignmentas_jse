package com.cLIGenericsPart15;

/*
 * Queue is a interface to store elements in sorting order and FIFO->PriortyQuque
 * Deque-> ArrayDeque-> LinkedList
 */
import java.util.*;

public class QueueClass {

	public static void main(String[] args) {
		Queue<String> queue = new PriorityQueue<>();
		queue.add("amani");
		queue.add("ammu");
		queue.add("ama");
		queue.add("abc");
		queue.remove();// fifo on the after sorting order
		System.out.println(queue);

		queue = new LinkedList<>();
		queue.add("amani");
		queue.add("ammu");
		queue.add("ama");
		queue.add("abc");
		queue.remove();
		System.out.println(queue);

		Deque<String> deque = new ArrayDeque<>();
		deque.add("amani");
		deque.add("ammu");
		deque.add("ama");
		queue.add("abc");
		deque.push("hello");
		System.out.println(deque.peek());
		System.out.println(deque);
	}

}
