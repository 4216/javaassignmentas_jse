package com.cLIGenericsPart15;

/*
 * map is a collection of key:value pair, insertion order is preserved,not allow duplicate key's allow the values
 * HashMap allow on null key
 * HashTable not allow the null and duplicate key'ss
 */
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapClass {

	public static void main(String[] args) {
		Map<Integer, String> map = new HashMap<>();
		map.put(101, "amani");
		map.put(102, "ammu");
		System.out.println(map);

		Set<Entry<Integer, String>> set = map.entrySet();
		Iterator<Entry<Integer, String>> it = set.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println(map.entrySet());

		System.out.println(map.keySet());

		Set<Integer> key = map.keySet();
		for (Integer s1 : key)
			System.out.println(map.get(s1));
	}

}
