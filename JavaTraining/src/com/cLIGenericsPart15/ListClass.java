package com.cLIGenericsPart15;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;

/*collections are used to collect the data.
 * Iterable is super interface of all the collections,
 * collection is interface in java
 * 			Collections in class java
 * List ->ArrayList,LinkedList,Vector->Stack
 * set ->TreeSet,HashSet,LinkedHashSet
 * Qeque ->PriortyQuque, LinkedList,
 * Dequeue-> ArrayDeque
 * Map ->HashMap,HashTable,LinkedHashMap,TreeMap
 * 
 * SortedSet,navigebleSet,SortedMap,
 */
/*List-> is used stored the elements in the insertion order(like sequence) and we can allow to store duplicate elements,
 * The list automatically grows if elements exceed initial size. 
 */
public class ListClass {

	public static void main(String[] args) {
		ListClass cc = new ListClass();
		System.out.println(cc.getClass());
		List<Integer> ls;
		ls = new ArrayList<>(2);
		ls.add(10);
		ls.add(11);
		ls.add(12);// automatically increased the size
		ls.remove(1);// 11 removed
		ls.add(13);
		ls.set(2, 99);// 13 changed to 99
		System.out.println(ls);
//LinkedList- is used to sort the element fast and move the elements, curd operation is fase compare to other class in linkedlist, is bind with previous and next node memory addresses
		ls = new LinkedList<>();
		ls.add(101);
		System.out.println(ls);

//vectoer is intoduced java 1.1 version, java 1.1 version all synchronized 
		ls = new Vector<>();
		ls.add(200);
		ls.add(201);
		System.out.println(ls);
//stack follows LIFO 
		ls = new Stack<>();
		ls.add(10);
		ls.add(20);
		System.out.println(ls);
		Stack<String> s = new Stack<>();
		s.push("amani");
		s.add("heelo");
		s.add("123");
		s.pop();

		System.out.println(s);
		System.out.println(s.peek());

	}

}
