package com.cLIGenericsPart15;

public class AutoBoxingUnBoxing {

	public static void main(String[] args) {
		Integer boxing1 = 10;// boxing
		int num = 20;

		Integer boxing2 = new Integer("30");// boxing

		Integer autoboxing1 = num;// autoboxing
		Integer autoboxing2 = Integer.valueOf(num);// autoboxing

		int unboxing1 = boxing1;// unboxing
		int unboxing2 = Integer.valueOf(boxing2);// unboxing

		System.out.println(autoboxing1 + "," + autoboxing2 + "," + unboxing1 + "," + unboxing2);
	}

}
