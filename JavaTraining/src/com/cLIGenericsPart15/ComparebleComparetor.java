package com.cLIGenericsPart15;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/*
 * Comparable and Comparator interface are used to sort collections
 * ->Both are implemented using generic's.
 * *Using the Comparable interface: Overrides the "compareTo" Method, Provides only one sort option
 * *Using the Comparator interface: is implemented by using the "compare" method, Enables you to create multiple Comparator classes,
 * Enable you to create and use numerous sorting options
 */

class ComparableStudent implements Comparable<ComparableStudent> {
	private int id = 0;
	private String name;
	private double gpa = 0.0;

	public ComparableStudent(int id, String name, double gpa) {
		super();
		this.id = id;
		this.name = name;
		this.gpa = gpa;
	}

	@Override
	public int compareTo(ComparableStudent o) {
		if (this.id > o.id) {
			return 1;
		} else if (this.id < o.id) {
			return -1;
		} else {
			return 0;
		}
	}

	@Override
	public String toString() {
		return "ComparableStudent [id=" + id + ", name=" + name + ", gpa=" + gpa + "]";
	}
}

class ComparaToStudent implements Comparator<ComparaToStudent> {
	private int id = 0;
	private String name;
	private double gpa = 0.0;

	public ComparaToStudent() {
	}

	public ComparaToStudent(int id, String name, double gpa) {
		super();
		this.id = id;
		this.name = name;
		this.gpa = gpa;
	}

	@Override
	public int compare(ComparaToStudent o1, ComparaToStudent o2) {
		if (o1.id > o2.id) {
			return 1;
		} else if (o1.id < o2.id) {
			return -1;
		} else {
			return 0;
		}
	}

	@Override
	public String toString() {
		return "ComparaToStudent [id=" + id + ", name=" + name + ", gpa=" + gpa + "]";
	}
}

public class ComparebleComparetor {

	public static void main(String[] args) {

		Set<ComparableStudent> set = new TreeSet<ComparableStudent>();
		set.add(new ComparableStudent(103, "amani", 3.9));
		set.add(new ComparableStudent(102, "abc", 4.9));
		set.add(new ComparableStudent(101, "ammu", 9.0));
		for (ComparableStudent c : set) {
			System.out.println(c);
		}

		List<ComparaToStudent> set1 = new ArrayList<ComparaToStudent>();
		set1.add(new ComparaToStudent(103, "amani", 3.9));
		set1.add(new ComparaToStudent(102, "abc", 4.9));
		set1.add(new ComparaToStudent(101, "ammu", 9.0));

		Comparator<ComparaToStudent> name = new ComparaToStudent();
		Collections.sort(set1, name);

		for (ComparaToStudent cc : set1) {
			System.out.println(cc);
		}

	}

}
