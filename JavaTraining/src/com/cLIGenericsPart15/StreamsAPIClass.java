package com.cLIGenericsPart15;

import java.util.ArrayList;
import java.util.List;

class Test {
	private int id;
	private String name;
	private int age;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Test(int id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Test [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
}

public class StreamsAPIClass {

	public static void main(String[] args) {
		List<Test> ls = new ArrayList<Test>();
		ls.add(new Test(101, "ab", 20));
		ls.add(new Test(102, "ab", 21));
		ls.add(new Test(103, "ab", 22));
		ls.add(new Test(104, "ab", 23));
		ls.add(new Test(105, "ab", 24));
		ls.add(new Test(106, "ab", 25));
		ls.add(new Test(107, "ab", 26));
		ls.add(new Test(108, "ab", 27));
		ls.add(new Test(109, "ab", 28));
		System.out.println(ls);
		ls.stream().filter(t -> t.getAge() >= 22 && t.getAge() <= 26).forEach(System.out::println);
	}
}
