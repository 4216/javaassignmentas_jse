package com.cLIGenericsPart15;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

/*
 * set is interface -alow only unique elements, not allow duplicate elements, order is also not there
 */
public class SetClass {

	public static void main(String[] args) {
		Set<String> set = new TreeSet<>();
		set.add("amani");
		set.add("ammu");
		set.add("one");
		set.add("three");
		System.out.println(set);

		set = new LinkedHashSet<>();// insertion order is reserved
		set.add("ammu");
		set.add("amani");
		set.add("one");
		set.add("three");
		System.out.println(set);

		set = new HashSet<>();
		set.add("amani");
		set.add("ammu");
		set.add("one");
		set.add("three");
		System.out.println(set);
	}

}
