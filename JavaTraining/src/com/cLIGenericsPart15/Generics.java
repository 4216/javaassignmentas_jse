package com.cLIGenericsPart15;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Amani
 * 
 */
 
/**
 * Generic's<> are introduce java 1.5 version, mainly used in Generic are type
 * safe Collection are not work primitive data type we are used wrapper class
 */
public class Generics {

	public static void main(String[] args) {
		List<String> ls = new ArrayList<>();
		ls.add("amani");
		ls.add("ammu");
		ls.add("sweety");
		System.out.println(ls.get(0));
		System.out.println(ls);
	}

}
