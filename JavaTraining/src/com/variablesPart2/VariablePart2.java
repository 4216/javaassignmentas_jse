package com.variablesPart2;

public class VariablePart2 {
	short b;// instance /object level (declared)
	static int c = 20;// class level
	float d = 2.5f;
	long e = 100L;
	double f = 22.5;
	String g = "amani";
	boolean h;

	public static void main(String[] args) throws CloneNotSupportedException {
		byte a = 10;// local variable
		System.out.println(a + "," + c);
		VariablePart2 obj = new VariablePart2();
		System.out.println(obj.b + "," + obj.d + "," + obj.e + "," + obj.f + "," + obj.g + "," + obj.h);

	}

}
