package com.collectionPart15;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class Emp {
	private int id;
	private String name;

	public Emp() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Emp(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Emp [id=" + id + ", name=" + name + "]";
	}

}

public class AllCollection {

	public static void main(String[] args) {
		Map<Integer, Emp> m = new HashMap<>();
		m.put(101, new Emp(1011, "amani"));
		m.put(102, new Emp(1012, "ab"));
		m.put(103, new Emp(1013, "amad"));
		System.out.println(m.get(101));
		Set<Entry<Integer, Emp>> s = m.entrySet();
		Iterator<Entry<Integer, Emp>> itr = s.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

}
