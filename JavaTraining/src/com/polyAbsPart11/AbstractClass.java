package com.polyAbsPart11;

abstract class A {
	public abstract void m1();

	void m2() {
		System.out.println("A.m2()");
	}
}

public class AbstractClass extends A {
	public static void main(String[] args) {
		A a = new AbstractClass();
		a.m1();
		a.m2();
	}

	@Override
	public void m1() {
		System.out.println("AbstractClass.m1()");

	}
}
