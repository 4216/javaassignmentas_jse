package com.polyAbsPart11;

class Mng {
	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		System.out.println(result);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mng other = (Mng) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Mng [id=" + id + ", name=" + name + "]";
	}

}

public class Emp {

	public static void main(String[] args) {
		Mng m = new Mng();
		m.setId(1);
		m.setName("ama");
		m.setId(2);
		m.setName("ammu");

		Mng m1 = new Mng();
		m1.setId(1);
		m1.setName("AMANI");
		m1.setId(2);
		m1.setName("ammu");

		System.out.println(m.hashCode());
		System.out.println(m.equals(m1));
	}

	static int mm() {
		main(new String[] {});// main method called
		System.out.println("........");
		return 99;
	}

}
