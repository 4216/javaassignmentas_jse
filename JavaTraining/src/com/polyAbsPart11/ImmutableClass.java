package com.polyAbsPart11;

final class B {
	public void m1() {
		System.out.println("A.m1()");
	}
}

class C// extends B
{
	public static void m2() {
		System.out.println("B.m2()");
	}
}

//main class can define final keyword
public final class ImmutableClass extends C {

	public static void main(String[] args) {
		C.m2();
	}

}
