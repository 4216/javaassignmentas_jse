package com.dataManipulationPart14;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

public class DateClass {

	public static void main(String[] args) {
		LocalDate date = LocalDate.now();
		System.out.println(date);
		LocalDate day = LocalDate.of(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));// set end user
		System.out.println(day);

		LocalDateTime dt = LocalDateTime.now();
		System.out.println(dt);
		int day1 = dt.getDayOfMonth();
		int month = dt.getMonthValue();
		int year = dt.getDayOfYear();

		Month month1 = dt.getMonth();
		int year1 = dt.getYear();

		System.out.println(day1);
		System.out.println(month1 + "--" + month);
		System.out.println(year + "--" + year1);
	}

}
