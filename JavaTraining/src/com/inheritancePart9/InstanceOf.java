package com.inheritancePart9;

class A {
	A m() {
		System.out.println("A.m1()");
		return new A();
	}
}

class B extends A {
	void m1() {
		System.out.println("B.m1()");
	}
}

public class InstanceOf {

	public static void main(String[] args) {
		B b = new B();
		b.m1();
		if (b instanceof A) {// instanceof operator is used to test whether the object os an instance of the
								// specified type(class/sub class/ interface)'it is also known as type
								// comparison operator because it compares it compares the instance with type.
								// it returns either true(or) false
			A a = ((A) b).m();
			System.out.println(a);
		} else {
			System.out.println("InstanceOf.main()");
		}
	}

}
