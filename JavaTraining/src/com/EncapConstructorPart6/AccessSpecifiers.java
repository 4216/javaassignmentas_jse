package com.EncapConstructorPart6;

class A {
//	there is only class level access
	private void m1() {
		System.out.println("A.m1()");
	}

//	there is access only with in package level
	void m2() {
		m1();
		System.out.println("A.m2()");
	}

//	there access with in out side the package in super class not access in child class
	protected void m3() {
		System.out.println("A.m3()");
	}

//	any where in access 
	public void m4() {
		System.out.println("A.enclosing_method()");
	}
}

class B extends A {
	A a = new A();
}

public class AccessSpecifiers {
	protected void l1() {
		System.out.println("AccessSpecifiers.l1()");
		B b = new B();
		b.m2();
		b.m3();
		b.m4();
	}

}
