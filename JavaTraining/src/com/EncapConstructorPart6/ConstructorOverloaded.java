package com.EncapConstructorPart6;

public class ConstructorOverloaded {
	@SuppressWarnings("unused")
	private int id;
	@SuppressWarnings("unused")
	private String name;

	public ConstructorOverloaded(int id) {
		super();
		this.id = id;
		System.out.println(id);

	}

	public ConstructorOverloaded(int id, String name) {
		this(id);

		this.id = id;
		this.name = name;
		System.out.println(id + "," + name);
	}

	public static void main(String[] args) {
		Encapsulation e = new Encapsulation();
		e.setId(102);
		e.setName("srikanth");
		new ConstructorOverloaded(e.getId(), e.getName());

	}

}
