package com.MethodsPart8;

public class Conditions {

	static public void m1(int a) {
		System.out.println(a);
	}

	static public void m1(int a, int b) {
		System.out.println(a + b);
	}

	static public void m1(int a, int b, float c) {
		int d = (int) c;
		System.out.println(a + b / d);
	}

	public static void main(String[] args) {
		m1(10);
		m1(10, 20);
		m1(10, 20, 30);
	}

}
