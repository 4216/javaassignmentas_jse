package com.streamPart17;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FirstInputOut {

	public static void main(String[] args) throws IOException {
		int a = 10, b = 20, c = 30;
		OutputStream os = new FileOutputStream("C:\\Training\\JavaTraining\\text.txt");
		os.write(a);
		os.write(b);
		os.write(c);
		System.out.println("SUCCESFULLY INSERTED...");
		os.close();

		InputStream is = new FileInputStream("C:\\Training\\JavaTraining\\text.txt");
		int aa = 0;
		while ((aa = is.read()) != -1) {
			System.out.println(aa);
		}
		is.close();

	}

}
