package com.streamPart17;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class A implements Serializable {
	private static final long serialVersionUID = -2456391727594853300L;

	int a;
	String name;

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public A(int a, String name) {
		super();
		this.a = a;
		this.name = name;
	}

	void setData() throws IOException {
		FileOutputStream fos = new FileOutputStream("C:\\Training\\JavaTraining\\text1.txt");
		ObjectOutputStream obj = new ObjectOutputStream(fos);
		obj.writeObject(new A(101, "amani"));
		System.out.println("success..");
		obj.close();

	}

	@Override
	public String toString() {
		return "A [a=" + a + ", name=" + name + "]";
	}

}

public class SerialDeserial {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		A a = new A(101, "sri");
		a.setData();

		FileInputStream fis = new FileInputStream("C:\\Training\\JavaTraining\\text1.txt");
		ObjectInputStream obj = new ObjectInputStream(fis);
		A aa = (A) obj.readObject();
		System.out.println(aa.getA() + "..." + aa.getName());
		obj.close();

	}

}
