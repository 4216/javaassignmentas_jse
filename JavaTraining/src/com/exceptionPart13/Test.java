package com.exceptionPart13;

/**
 * 
 * @author Amani
 *
 */
 
class AA {
	/*
	 * Propagation is possible only throws block and catch we can Handle the
	 * exception and execute the remaining code
	 */
	void m1(int a, int b) throws ArithmeticException {
		try {
			System.out.println("A.m1()");
			m2(a, b);
			System.out.println("A.m1()");
		} catch (ArithmeticException e) {
			e.printStackTrace();
		}
	}

	void m2(int a, int b) throws ArithmeticException {
		System.out.println("A.m2()");
		System.out.println(a / b);
		System.out.println("A.m2()");
	}
}

public class Test {

	public static void main(String[] args) throws ArithmeticException {
		AA a = new AA();
		System.out.println("Test.main()");
		a.m1(10, 0);
		System.out.println("Test.main()");
	}

}
