package com.exceptionPart13;

class A extends Error {
	private static final long serialVersionUID = 1L;

	void m1() {
		System.out.println("A.m1()");
	}
}

class B extends Exception {
	private static final long serialVersionUID = 1L;

	public B(String name) {
		super(name);
	}
}

class C extends B {
	private static final long serialVersionUID = 6212705999585691625L;

	public C(String name) {
		super(name);
	}

	void m2(String name) throws B {
		System.out.println("B.m2()" + name);
	}
}

public class ExceprionAll {

	public static void main(String[] args) throws B {
		System.out.println("ExceprionAll.main()");
		C b = new C("Srikanth");
		b.m2("Sri");
		throw new C("Srikanth");

	}

}
