package com.interfacePart10;

@FunctionalInterface
interface A {
	void m1();
}

class B implements A {
	public void m1() {
		System.out.println("B.LambdaExprestion.enclosing_method()");
	}
}

public class LambdaExprestion {
	public static void main(String[] args) {
		A a = () -> System.out.println("A ...");
		a.m1();
	}

}
