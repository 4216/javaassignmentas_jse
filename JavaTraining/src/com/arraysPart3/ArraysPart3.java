package com.arraysPart3;

public class ArraysPart3 {

	public static void main(String[] args) {
		int a[] = { 10, 20, 30, 40 };
		if (a.length != 0) {
			for (int i : a) {
				System.out.println(i);
			}
		}

		int aa[][] = new int[3][];
		aa[0] = new int[2];
		aa[1] = new int[3];
		aa[2] = new int[1];
		for (int i = 0; i < aa.length; i++) {
			for (int j = 0; j < aa[i].length; j++) {
				System.out.print(i + "" + j + " ");
			}
			System.out.println();
		}
	}

}
