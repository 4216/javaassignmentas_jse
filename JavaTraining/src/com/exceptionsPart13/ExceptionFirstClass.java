package com.exceptionsPart13;

/**
 * 
 * @author Amani
 *
 */
 
/*
 * try{} is a block ro write the risky code catch(Ex e){} is a method to handle
 * the exception finally {} is a block to clean up activity always execute throw
 * new EX(); is use to generate the exception throws EX -> forward the exception
 * and this work method level
 */
class A extends Exception {
	private static final long serialVersionUID = -4495695249267735913L;

	public A(String message) {
		super(message);
	}

}

class B extends RuntimeException {
	private static final long serialVersionUID = 5897213593610363662L;

	public B(String message) {
		super(message);
	}
}

class C extends A {
	private static final long serialVersionUID = 396896983205756622L;

	public C(String message) {
		super(message);
	}
}

public class ExceptionFirstClass {

	public static void main(String[] args) {
		try {
			throw new C("hello");
		} catch (C e) {
			System.err.println(e.toString());
		}
	}

}
