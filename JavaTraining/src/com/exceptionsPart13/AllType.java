package com.exceptionsPart13;

import java.util.Scanner;

public class AllType {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a numbers..");
		try {
			int num1 = Integer.parseInt(sc.next());
			int num2 = Integer.parseInt(sc.next());
			System.out.println(num1 / num2);
		} catch (ArithmeticException | NullPointerException | ArrayIndexOutOfBoundsException
				| NumberFormatException e) {
			e.printStackTrace();
		} finally {
			sc.close();
		}

	}

}
