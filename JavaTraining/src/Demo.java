import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * 
 * @author Amani
 *
 */
 
class A implements Runnable {
	public void run() {
		System.out.println("A.run()");
	}

}

public class Demo extends Thread {
	public void run() {
		System.out.println("run");
	}

	public static void main(String[] args) {
		System.out.println("Demo.main()");

		Demo d = new Demo();
		d.start();

		A a = new A();
		Thread t = new Thread(a);
		t.start();

		List<String> ll = Arrays.asList("amani", "ammu", "hello");
		/*
		 * List.of() is java 9 featur List<String> l=List.of("");
		 */
		System.out.println(ll.stream().filter(ll2 -> ll2.equals("amani")));

		int i = IntStream.rangeClosed(1, 5).parallel().reduce(0, (sum, element) -> sum + element);
		System.out.println("Result: " + i);
	}

}
