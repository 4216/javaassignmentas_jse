package com.model;

/**
 * @author Amani
 */
 
import java.io.Serializable;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;




public class Address implements Serializable, InitializingBean, DisposableBean {
	/**
	 * The serialVersionUID attribute is an identifier that is used to
	 * serialize/deserialize an object of a Serializable class.
	 */
	private static final long serialVersionUID = 7207449881961255244L;

	private int doorNO;
	private String city;
	private String state;
	
	

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void start() {
		System.out.println("Address->Start");
	}

	public int getDoorNO() {
		return doorNO;
	}

	public void setDoorNO(int doorNO) {
		this.doorNO = doorNO;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Address(int doorNO, String city, String state) {
		super();
		this.doorNO = doorNO;
		this.city = city;
		this.state = state;
	}

	public void stop() {
		System.out.println("Address.stop()");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		start();
	}

	@Override
	public void destroy() throws Exception {
		stop();
	}
}
