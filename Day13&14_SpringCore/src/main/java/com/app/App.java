package com.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Employee;

/**
 * 
 * @author Amani
 *
 */
 
public class App {
	public static void main(String[] args) {

//		create IOC container

		/*
		 * Resource resource = new
		 * ClassPathResource("com/sri/config/applicationConfig.xml");
		 * 
		 * @SuppressWarnings("deprecation") BeanFactory factory = new
		 * XmlBeanFactory(resource);
		 * 
		 * Employee employee = factory.getBean("employee", Employee.class);
		 * System.out.println(employee);
		 */
//		create IOC container
//
		ApplicationContext ctx = new ClassPathXmlApplicationContext("com/amani/config/applicationConfig.xml");
		Employee employee = ctx.getBean("employee", Employee.class);
		System.out.println(employee.hashCode());

		Employee employee1 = ctx.getBean("employee1", Employee.class);
		System.out.println(employee1.hashCode());
		/*
		 * employee.setEmpName("amani"); employee.setSalary(201.02f);
		 * System.out.println(employee);
		 * 
		 * // Address address=(Address)ctx.getBean("addr"); Address address =
		 * ctx.getBean("addr", Address.class); System.out.println(address);
		 * 
		 * Date d = (Date) ctx.getBean("dt"); System.out.println("date ::" + d);
		 * 
		 * Printer printer1 = Printer.getInstance();
		 * System.out.println(printer1.hashCode()); printer1.printData("hello1");
		 * 
		 * Printer printer2 = Printer.getInstance();
		 * System.out.println(printer2.hashCode()); printer2.printData("hello2");
		 */
		((AbstractApplicationContext) ctx).close();

	}
}
