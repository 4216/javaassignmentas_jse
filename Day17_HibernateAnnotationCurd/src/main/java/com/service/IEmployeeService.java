package com.service;

import java.util.List;

import com.model.Employee;

public interface IEmployeeService {
	public Employee createEmploye(Employee employee);

	public List<Employee> readAllEmployees();

	public Employee readEmployeeById(Integer empId);

	public Employee readEmployeeByName(String name);

	public Employee updateEmployee(Employee em);

	public Integer delete(int id);
}
