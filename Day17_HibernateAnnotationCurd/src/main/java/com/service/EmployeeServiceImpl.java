package com.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.dao.EmployeeDaoImpl;
import com.dao.IEmployeeDao;
import com.model.Employee;
import com.util.HibernateConnection;

public class EmployeeServiceImpl implements IEmployeeService {

	IEmployeeDao dao = new EmployeeDaoImpl();
	Session session = HibernateConnection.getSessionFactory().getCurrentSession();
	Transaction tx;

	public Employee createEmploye(Employee employee) {
		tx = session.beginTransaction();
		Employee emp = dao.createEmploye(employee, session);
		if (emp != null) {
			tx.commit();
		} else {
			tx.rollback();
		}
		return emp;
	}

	public List<Employee> readAllEmployees() {
		tx = session.beginTransaction();
		return dao.readAllEmployees(session);
	}

	public Employee readEmployeeById(Integer empId) {
		tx = session.beginTransaction();
		return dao.readEmployeeById(empId, session);
	}

	public Employee readEmployeeByName(String name) {
		tx = session.beginTransaction();
		Employee employee = dao.readEmployeeByName(name, session);
		return employee;
	}

	public Employee updateEmployee(Employee em) {
		tx = session.beginTransaction();
		return dao.updateEmployee(em, session);

	}

	public Integer delete(int id) {
		tx = session.beginTransaction();
		Integer a = dao.delete(id, session);
		tx.commit();
		return a;
	}

}
