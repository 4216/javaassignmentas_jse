package com.dao;

import java.util.List;

import org.hibernate.Session;

import com.model.Employee;

public class EmployeeDaoImpl implements IEmployeeDao {

	public Employee createEmploye(Employee employee, Session session) {
		Employee employee2 = (Employee) session.save(employee);
		return employee2;
	}

	public List<Employee> readAllEmployees(Session session) {
		String readAllEmployee = "From Employee";
		return session.createQuery(readAllEmployee).list();
	}

	public Employee readEmployeeById(Integer empId, Session session) {
		return session.load(Employee.class, empId);
	}

	public Employee readEmployeeByName(String empName, Session session) {
		Employee ee = new Employee();
		ee.setEmpName(empName);
		Employee e = session.find(Employee.class, ee);
		return e;
	}

	public Employee updateEmployee(Employee em, Session session) {
		session.update(em);
		return em;
	}

	public Integer delete(Integer id, Session session) {
		Employee e = new Employee();
		e.setEmpNo(id);
		session.delete(e);
		return id;
	}
}
