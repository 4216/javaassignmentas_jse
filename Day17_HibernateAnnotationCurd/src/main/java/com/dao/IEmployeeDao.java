package com.dao;

import java.util.List;

import org.hibernate.Session;

import com.model.Employee;

public interface IEmployeeDao {
	public Employee createEmploye(Employee employee, Session session);

	public List<Employee> readAllEmployees(Session session);

	public Employee readEmployeeById(Integer empId, Session session);

	public Employee readEmployeeByName(String name, Session session);

	public Employee updateEmployee(Employee em, Session session);

	public Integer delete(Integer id, Session session);
}
