package com.app;

import java.util.List;

import com.model.Employee;
import com.service.EmployeeServiceImpl;
import com.service.IEmployeeService;

/**
 * 
 * @author Amani
 *
 */
 
public class App {
	public static void main(String[] args) {
		IEmployeeService dao = new EmployeeServiceImpl();
		Employee employee = new Employee("Amani", 3000f);
//		dao.createEmploye(employee);

		List<Employee> emp = dao.readAllEmployees();
		for (Employee e : emp) {
			System.out.println(e);
		}

//		Employee employee = dao.readEmployeeById(1);
//		System.out.println(employee);

//		Employee employee = dao.readEmployeeByName("amani");
//		System.out.println(employee);

//		Employee employee = new Employee(1, "amani", 200f);
//		Employee e = dao.updateEmployee(employee);
//		System.out.println(e);

//		Integer a = dao.delete(1);
//		System.out.println(a);

//		IEmployeeDao dao = new EmployeeDaoImpl();
//		Employee employee = new Employee("amani", 3000f);
//		dao.createEmploye(employee);

//		List<Employee> emp = dao.readAllEmployees();
//		for (Employee e : emp) {
//			System.out.println(e);
//		}

//		Employee employee = dao.readEmployeeById(1);
//		System.out.println(employee);

//		Employee employee = dao.readEmployeeByName("amani");
//		System.out.println(employee);

//		Employee employee = new Employee(1, "amani1", 200f);
//		Employee e = dao.updateEmployee(employee);
//		System.out.println(e);

//		Integer a = dao.delete(1);
//		System.out.println(a);
	}
}
