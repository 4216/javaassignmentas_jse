package com.app;

import java.util.Iterator;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.model.Answer;
import com.model.Question;
import com.util.HibernateConnection;

/**
 * 
 * @author Amani
 *
 */

public class ManyToManyMapping {

	public static void main(String[] args) throws HibernateException {

		Session session = HibernateConnection.getSessionFactory().getCurrentSession();
		try {
			Transaction tx = session.beginTransaction();
			/*
			 * Answer answer1 = new Answer();
			 * answer1.setAnswername("Java is a programming language");
			 * answer1.setPostedBy("Amani");
			 * 
			 * Answer answer2 = new Answer(); answer2.setAnswername("Java is a platform");
			 * answer2.setPostedBy("ammu");
			 * 
			 * ArrayList<Answer> l1 = new ArrayList<Answer>(); l1.add(answer1);
			 * l1.add(answer2);
			 * 
			 * Question question1 = new Question(); question1.setQname("What is Java?");
			 * question1.setAnswers(l1);
			 * 
			 * Answer answer3 = new Answer();
			 * answer3.setAnswername("Servlet is an Interface");
			 * answer3.setPostedBy("sweety");
			 * 
			 * Answer answer4 = new Answer(); answer4.setAnswername("Servlet is an API");
			 * answer4.setPostedBy("A");
			 * 
			 * ArrayList<Answer> l2 = new ArrayList<Answer>(); l2.add(answer3);
			 * l2.add(answer4);
			 * 
			 * Question question2 = new Question(); question2.setQname("What is Servlet?");
			 * question2.setAnswers(l2);
			 * 
			 * session.save(question1); session.save(question2);
			 */

//			Question list1 = session.load(Question.class, 1);
//			System.out.println(list1);

			TypedQuery query = session.createQuery("from Question");
			List<Question> list = query.getResultList();
			if (list != null) {
				Iterator<Question> itr = list.iterator();
				while (itr.hasNext()) {
					Question q = itr.next();
					System.out.println(q.getId() + " " + q.getQname() + " " + q.getAnswers());
					List<Answer> aa = q.getAnswers();
					aa.forEach(a -> System.out.println(a.getId() + " " + a.getPostedBy() + " " + a.getAnswername()));
				}
			}

			tx.commit();
			HibernateConnection.shutdown();// close the connection
			System.out.println("closed the connection");
		} catch (Exception e) {
			System.out.println("error...");
		}
	}

}
