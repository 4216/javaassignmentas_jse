package com.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;



@Entity

public class Question {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String qname;

	@ManyToMany(targetEntity = Answer.class, cascade = { CascadeType.ALL })
	@JoinTable(name = "q_ans1123", joinColumns = { @JoinColumn(name = "q_id") }, inverseJoinColumns = {
			@JoinColumn(name = "ans_id") })
//	@JoinColumns(@JoinColumn(name = "q_id"))
	private List<Answer> answers;
}