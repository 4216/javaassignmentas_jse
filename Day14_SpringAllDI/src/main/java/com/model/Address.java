package com.model;

/**
 * @author Amani
 */

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Address implements Serializable {
	/**
	 * The serialVersionUID attribute is an identifier that is used to
	 * serialize/deserialize an object of a Serializable class.
	 */
	private static final long serialVersionUID = 7207449881961255244L;

	private Integer doorNO;
	private Set<String> city;
	private List<String> state;
	private Map<Integer, String> add;

	public Address() {
		super();
		
	}

	public Address(Integer doorNO, Set<String> city, List<String> state, Map<Integer, String> add) {
		super();
		this.doorNO = doorNO;
		this.city = city;
		this.state = state;
		this.add = add;
	}

	public Integer getDoorNO() {
		return doorNO;
	}

	public void setDoorNO(Integer doorNO) {
		this.doorNO = doorNO;
	}

	public Set<String> getCity() {
		return city;
	}

	public void setCity(Set<String> city) {
		this.city = city;
	}

	public List<String> getState() {
		return state;
	}

	public void setState(List<String> state) {
		this.state = state;
	}

	public Map<Integer, String> getAdd() {
		return add;
	}

	public void setAdd(Map<Integer, String> add) {
		this.add = add;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
