package com.model;

import java.io.Serializable;




public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	private int empNo;
	private String empName;
	private float salary;
	private Address address;
	
	
	
	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee(int empNo, String empName, float salary, Address address) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.salary = salary;
		this.address = address;
	}

	public Employee() {
		super();
		
	}

	public void start() {
		System.out.println("Employee.start()");
	}
	
	public void stop() {
		System.out.println("Employee.start()");
	}
}
