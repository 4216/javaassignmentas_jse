package com.App;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Address;
import com.model.Employee;

/**
 * 
 * @author Amani
 *
 */
 
public class App {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("com/sri/config/applicationConfig.xml");

		Employee employee = ctx.getBean("employee", Employee.class);
		System.out.println(employee);

		Address address = ctx.getBean("addr", Address.class);
		System.out.println(address);
		
		((AbstractApplicationContext) ctx).close();
	}
}
