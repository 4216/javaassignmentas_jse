package com.model;

import java.io.Serializable;

public class Organization implements Serializable {
	private static final long serialVersionUID = -6668577942019241831L;
	private Employee employee;
	private String address;
	private Department department;

	public Organization() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Organization(Employee employee, String address, Department department) {
		super();
		this.employee = employee;
		this.address = address;
		this.department = department;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
