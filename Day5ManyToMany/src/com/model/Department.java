package com.model;

import java.io.Serializable;

public class Department implements Serializable {
	private static final long serialVersionUID = -676044875032032966L;
	private Integer dptId;
	private String role;

	public Department() {
		super();

	}

	public Department(Integer dptId, String role) {
		super();
		this.dptId = dptId;
		this.role = role;
	}

	public Integer getDptId() {
		return dptId;
	}

	public void setDptId(Integer dptId) {
		this.dptId = dptId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
