package com.model;

import java.io.Serializable;

/**
 * 
 * @author Amani
 *
 */

public class Employee implements Serializable {
	private static final long serialVersionUID = 8325171485438216917L;

	private Integer id;
	private String name;

	public Employee() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Employee(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

}
