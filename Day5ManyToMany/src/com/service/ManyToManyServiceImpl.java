package com.service;

import com.model.Department;
import com.model.Employee;
import com.model.Organization;
import com.serviceImpl.IManyToManyService;

public class ManyToManyServiceImpl implements IManyToManyService {
	@Override
	public Organization getData(String address1) {
		Department department[] = new Department[3];
		department[0] = new Department(1, "software engineer");
		department[1] = new Department(2, "software Tester");
		department[2] = new Department(3, "Network");

		String address[] = new String[3];
		address[0] = "hyd";
		address[1] = "telangana";
		address[2] = "pune";

		Employee employee[] = new Employee[3];
		employee[0] = new Employee(101, "Amani");
		employee[1] = new Employee(102, "sweety");
		employee[2] = new Employee(103, "ammu");

		Organization organization[] = new Organization[3];
		organization[0] = new Organization(employee[0], address[0], department[0]);
		organization[1] = new Organization(employee[1], address[1], department[1]);
		organization[2] = new Organization(employee[2], address[2], department[2]);

		for (int i = 0; i < organization.length; i++) {
			if (address1.equals(address[i])) {
				organization[i].getEmployee();
				organization[i].getDepartment();
				organization[i].getAddress();
				return organization[i];
			}
		}
		return null;
	}
}
