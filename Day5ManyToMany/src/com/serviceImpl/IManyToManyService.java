package com.serviceImpl;

import com.model.Organization;

public interface IManyToManyService {
	public Organization getData(String address);
}
