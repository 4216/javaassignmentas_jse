package com.controler;

import com.model.Organization;
import com.service.ManyToManyServiceImpl;

/**
 * 
 * @author Amani
 *
 */
 
public class ManyToMany {

	public static void main(String[] args) {
		Organization organization = new Organization();

		ManyToManyServiceImpl service = new ManyToManyServiceImpl();
		organization = service.getData("hyd");
		try {
			if (organization.equals(null)) {
				System.out.println("Not found the data...");
			} else {
				System.out.println(organization.getEmployee().getName() + " Stay in " + organization.getAddress()
						+ " working in hcl as a " + organization.getDepartment().getRole());
			}
		} catch (Exception e) {
			System.out.println("Not found the data...");
		} finally {
			organization = null;
			service = null;
		}
	}
}
