package com.model;

public class Employee1 {

	private int empId;
	private String empName;
	private Address[] address;


	 public Employee1(int empId, String empName, Address[] address) {
	super();
	this.empId = empId;
	this.empName = empName;
	this.address = address;
	}

	 public int getEmpId() {
	return empId;
	}


	 public String getEmpName() {
	return empName;
	}


	 public Address[] getAddress() {
	return address;
	}

	 public void setAddress(Address[] address) {
	this.address = address;
	}
}
