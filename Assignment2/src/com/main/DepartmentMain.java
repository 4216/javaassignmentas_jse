package com.main;

import com.model.Address;
import com.model.Department;
import com.model.Employee1;
import com.service.EmployeeService;

public class DepartmentMain {

	public static void main(String[] args) {
		Address address = new Address("Kodaikanal", "TamilNadu", "Dindigul");
		Address address1 = new Address("Chennai", "TamilNadu", "Kancheepuram ");
		Address address2 = new Address("Vellore", "TamilNadu", "Vellore");
		Address address3 = new Address("Chennai", "TamilNadu", "Kancheepuram ");
		Address address4 = new Address("Trichi", "TamilNadu", "Trichi");
		Address address5 = new Address("Chennai", "TamilNadu", "Kancheepuram ");
		Address[] addresses = new Address[2];
		addresses[0] = address;
		addresses[1] = address1;
		Address[] addresses1 = new Address[2];
		addresses1[0] = address2;
		addresses1[1] = address3;
		Address[] addresses2 = new Address[2];
		addresses2[0] = address4;
		addresses2[1] = address5;
		Employee1 employee = new Employee1(1111, "Amani", addresses);
		Employee1 employee1 = new Employee1(2222, "lahari", addresses1);
		Employee1 employee2 = new Employee1(3333, "yamuna", addresses2);
		

		 EmployeeService employeeService = new EmployeeService();
		Employee1[] devEmp = new Employee1[2];
		devEmp[0] = employee;
		devEmp[1] = employee1;
		Employee1[] testEmp = new Employee1[1];

		 testEmp[0] = employee2;
		//testEmp[1] = employee3;

		 Department devDept = new Department("Developing", devEmp);
		Department tesDept = new Department("tester", testEmp);

		 /*
		* Employee1[] devArray = devDept.getEmployee(); Employee1[] tesArray =
		* tesDept.getEmployee(); Employee1[] trainArray = trainDept.getEmployee();
		*/

		 Employee1[] devArray = employeeService.displayDeveloper(devDept);
		Address[] devAddress = employeeService.displayDevAddress(devArray);
		for (int i = 0; i < devArray.length; i++) {
		System.out.println("\n"+"Employee Name :" + devArray[i].getEmpName() + "\n and Id :" + devArray[i].getEmpId()
		+ " in Development."+"\n");
		if(devArray[i].getEmpName().equals("Rekha")) {
		for (int j = 0; j < devAddress.length; j++) {
		if( devAddress[j].getCity().equals("Kodaikanal")) {
		System.out.print("who lives in " + devAddress[j].getCity() + " City " + devAddress[j].getDistrict()
		+ " District " + devAddress[j].getState() + " State.");

		 } else {
		System.out.print("who works in " + devAddress[j].getCity() + " City " + devAddress[j].getDistrict()
		+ " District " + devAddress[j].getState() + " State.");

		 }
		}
		}

		 }


		 try {
		Employee1[] testArray = employeeService.displayTester(tesDept);
		Address[] testAddress = employeeService.displayTestAddress(testArray);

		 for (int i = 0; i < testArray.length; i++) {
		System.out.println("Employee Name :" + testArray[i].getEmpName() + "\n and Id :"
		+ testArray[i].getEmpId() + " in Testing"+"\n");
		for (int j = 0; j < testAddress.length; j++) {

		 System.out.print("who works at " + testAddress[j].getCity() + " City " + testAddress[j].getDistrict()
		+ " District " + testAddress[j].getState() + " State.");

		 }
		}
		} catch (NullPointerException e) {
		System.out.println("No Employee in Testing");
		}

		 /*
		* for (int i = 0; i < trainArray.length; i++) {
		* System.out.println("Employee Name :" + trainArray[i].getEmpName() +
		* "\n and Id :" + trainArray[i].getEmpId() + " in Training"); }
		*/
		employee = null;
		employee1 = null;
		employee2 = null;
		//employee3 = null;
		employeeService = null;
		devEmp = null;
		testEmp = null;
		devDept = null;
		tesDept = null;

		 }


	}


