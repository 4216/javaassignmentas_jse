package com.exception;

/**
 * 
 * @author Amani
 *
 */
 
public class UserException extends Exception {

	private static final long serialVersionUID = 1571290720689479404L;
	String str;

	public UserException(String str) {
		super(str);
		this.str = str;
	}
}
