package com.App;

import java.sql.SQLException;
import java.util.List;

import com.dto.UserDTO;
import com.exception.UserException;
import com.service.IUserService;
import com.serviceImpl.UserServiceImpl;
import com.sri.vo.UserVO;

/**
 * 
 * @author Amani
 *
 */
 
/*
 * Sap id>=5digits password>5character fetch "username" from data base
 * 
 * o/p Welcome +"username" com.model->UserClass-> sapid,password,userName
 * com.dao->IUserDao(curd)->UserDaoImpl com.service->IUserService(where
 * validation should be done) com.exception->UserDefinedException com.main->main
 */
public class App {
	public static void main(String[] args) throws SQLException {
		IUserService service = new UserServiceImpl();

		try {
			List<UserDTO> userDataDto = service.retrivUser(new UserVO(101, "Amani"));
			if (userDataDto.isEmpty()) {
				System.out.println("PLEASE ENTER A VALID DATA");
			} else {
				for (UserDTO dto : userDataDto) {
					System.out.println("WELCOME  " + dto.getName().toUpperCase());
				}
			}
		} catch (UserException e) {
			System.err.println(e.toString());
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {

			service = null;
		}
	}
}
