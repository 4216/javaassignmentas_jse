package com.sri.vo;

import java.io.Serializable;




public class UserVO implements Serializable {
	private static final long serialVersionUID = -16338783199348857L;

	private Integer sapId;
	private String name;
	private String password;
	public UserVO() {
		super();
		
	}
	public UserVO(Integer sapId, String name, String password) {
		super();
		this.sapId = sapId;
		this.name = name;
		this.password = password;
	}
	public UserVO(int i, String string) {
		
	}
	public Integer getSapId() {
		return sapId;
	}
	public void setSapId(Integer sapId) {
		this.sapId = sapId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
