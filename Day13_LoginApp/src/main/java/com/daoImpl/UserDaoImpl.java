package com.daoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import com.bo.UserBO;
import com.dao.IUserDao;
import com.dto.UserDTO;
import com.util.DBConnection;

public class UserDaoImpl implements IUserDao {
	private static Connection con;
	static PreparedStatement ps;
	int i;
	private final static String Select_Query = "SELECT SAPID,NAME,PASSWORD FROM USER WHERE SAPID=? AND PASSWORD=?";
	private final static String insert_Query = "INSERT INTO USER (NAME, PASSWORD) VALUES (?,?)";
	private final static String Delete_Query = "DELETE FROM USER WHERE SAPID=? AND PASSWORD=?";
	private final static String Update_Query = "UPDATE USER SET NAME=?,PASSWORD=? WHERE SAPID=?";

	public ArrayList<UserBO> lbo;

	// Encryption
	private String getEncoded(String name) {
		return Base64.getEncoder().encodeToString(name.getBytes());
	}

	// Description
	private String getDecoded(String name) {
		return new String(Base64.getDecoder().decode(name)); 
	}

	public List<UserBO> retrivUser(UserBO bo) throws SQLException {
		lbo = new ArrayList<UserBO>();
		con = DBConnection.getConnection();
		ps = con.prepareStatement(Select_Query);
		if (ps != null) {
			ps.setInt(1, bo.getSapId());
			ps.setString(2, bo.getPassword());
			ResultSet rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					if (rs.getInt(1) == bo.getSapId() && rs.getString(3).equalsIgnoreCase(bo.getPassword())) {
						bo.setName(rs.getString(2));
						lbo.add(bo);
					} else {
						lbo = null;
					}
				}
			}
		}
		return lbo;
	}

	public int registerUser(UserBO bo) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int deleteUser(UserBO bo) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int updateUser(UserBO bo) {
		// TODO Auto-generated method stub
		return 0;
	}

}
