package com.dao;

import java.sql.SQLException;
import java.util.List;

import com.bo.UserBO;

public interface IUserDao {
	public List<UserBO> retrivUser(UserBO bo) throws SQLException;

	public int registerUser(UserBO bo);

	public int deleteUser(UserBO bo);

	public int updateUser(UserBO bo);
}
