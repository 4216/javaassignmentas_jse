package com.serviceImpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bo.UserBO;
import com.dao.IUserDao;
import com.daoImpl.UserDaoImpl;
import com.dto.UserDTO;
import com.exception.UserException;
import com.service.IUserService;
import com.sri.vo.UserVO;

public class UserServiceImpl implements IUserService {
	IUserDao dao = new UserDaoImpl();
	List<UserDTO> userDTO = new ArrayList<>();

	public List<UserDTO> retrivUser(UserVO vo) throws SQLException, UserException, Exception {
		if (validateUser(vo)) {
			List<UserBO> userDataBO = dao.retrivUser(new UserBO(vo.getSapId(), vo.getPassword()));
			for (UserBO bo : userDataBO) {
				UserDTO dto = new UserDTO();
				dto.setName(bo.getName());
				userDTO.add(dto);
			}
		} else {
			userDTO = null;
		}
		return userDTO;
	}

	public int registerUser(UserVO vo) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int deleteUser(UserVO vo) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int updateUser(UserVO vo) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean validateUser(UserVO vo) throws UserException {
		boolean flag = false;
		if (String.valueOf(vo.getSapId()).length() >= 2 && vo.getPassword().length() >= 5) {
			flag = true;
		} else {
			flag = false;
			throw new UserException("ENTER MiNIMUM SAPID >=2 AND Password >=5");
		}
		return flag;
	}

}
