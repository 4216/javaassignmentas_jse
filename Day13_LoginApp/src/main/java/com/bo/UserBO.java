package com.bo;

import java.io.Serializable;

public class UserBO implements Serializable {
	private static final long serialVersionUID = 5677445015144699552L;

	private Integer sapId;
	private String name;
	private String password;

	public UserBO() {
		super();

	}

	public UserBO(Integer sapId, String name, String password) {
		super();
		this.sapId = sapId;
		this.name = name;
		this.password = password;
	}

	public UserBO(Integer sapId2, String password2) {
		
	}

	public Integer getSapId() {
		return sapId;
	}

	public void setSapId(Integer sapId) {
		this.sapId = sapId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
