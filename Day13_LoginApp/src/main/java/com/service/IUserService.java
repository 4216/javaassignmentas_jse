package com.service;

import java.sql.SQLException;
import java.util.List;

import com.dto.UserDTO;
import com.exception.UserException;
import com.sri.vo.UserVO;

public interface IUserService {
	public List<UserDTO> retrivUser(UserVO vo) throws SQLException, UserException, Exception;

	public int registerUser(UserVO vo);

	public int deleteUser(UserVO vo);

	public int updateUser(UserVO vo);

	public boolean validateUser(UserVO vo) throws UserException;
}
