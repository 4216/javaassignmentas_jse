package mainJava8;

import com.demojava8.InterfaceImpl;
import com.demojava8.MyInteface;

public class MainJava8 {

	public static void main(String[] args) {
		
		//interface var = new imp()
		
		MyInteface myInteface = new InterfaceImpl();//instance ==object
		
		 int ans = myInteface.add(4, 23); // down casted == Tightly coupled with data type
		 System.out.println("Add of 2 number : " +ans);
		 System.out.println("Static final variable " + MyInteface.PI); // Class level == classname. variable why we call class level without instalization we can excute
		 System.out.println("Subtract : " +myInteface.sub(45, 12));
            //because it is static classname.variable == classname . method()
		 System.out.println("Multiplication " +MyInteface.mul(2, 7));
		 
	}

}
