package com.demojava8;

public interface MyInteface {
	
	public static final float PI = 3.1415f; // coding standard final variable are in captial
	public abstract int add(int a , int b); // method signature == method declaration == prototype
	
	public default int sub(int a , int b) {//method declaration +defination == logic  backward comptability
	return a - b;
	}
	
	public static int mul(int a , int b) { // method 
		return a*b;
	}
}
